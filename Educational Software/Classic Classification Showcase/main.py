#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Educational Software for Classic Machine Learning Methods
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

#Kivy and MPL imports
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy_garden.graph import MeshLinePlot
from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label
from kivy.uix.button import Button    
from kivy.uix.popup import Popup
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen
import kivy.properties as kyprops
from kivy.uix.image import Image

from matplotlib import pyplot as plt
from matplotlib import use as mpl_use
from matplotlib import rcParams
import seaborn as sn
import pandas as pd
mpl_use('module://kivy.garden.matplotlib.backend_kivy')
#Imports Gerais 
import os
import random
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'
import numpy as np
from functools import partial
import threading
import random
#Machine Learning Imports
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix, precision_recall_curve, classification_report
#Imports Especificos
from Data_Handler import Data_Handler # type: ignore
from graph_plots.MGraph.scatter2DGraph import Scatter2DGraph # type: ignore
   

class MenuScreen(Screen):

    #StringProperty definitons
    loadfile = kyprops.ObjectProperty(None)
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(MenuScreen, self).__init__(**kwargs)
        self.filer = (" \n")
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        self.helpt = ("[size=20][b]Classic Classification Schemes Showcase[/b][/size]\n\n"
        "[size=18][b]Introduction: [/b][/size]\n This program is a Showcase of the Classic Classification Methodology. If you don`t know what classic classification is we reffer you to:\n"
        "https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/wiki/Home\n If you are not familiar with the concepts we belive that reading said reference is important to fully take advantage of this showcase.\n"
        "This Small program will guide you throught each of the main steps of a classical classification pipeline for time signals, namely:\n"
        "Dataset Selection and Epoching, Preprocessing and Filtering, Feature Selection and extraction, Classificator selection and training, Evaluation and testing\n\n"
        "[size=18][b]Dataset Selection Screen: [/b][/size]\n"
        "In this screen you can see an imaging describing the five stepts on the Classification Pipeline:."
        "\n Myo Dataset - Load the Myoeletric Data"
        "\n Preprocessing - Apply some functions on the signal so it can be better classified"
        "\n Feature Extraction - Select some Functions that can extract information from the examples reducing the dimentionality of the problem"
        "\n Classification - Select and train the Classic model"
        "\n Gesture Output - Test the trained model on single example and see some general metrics for performance"
        "\n\nUse the button `Select Dataset` button to select a dataset file. The dataset should follow the same model as the others on the repository."
        "\n After that Select the `Next Step` buttom on the bottom of the screen to load the data and move on the next step.")
        Window.bind(mouse_pos=self.Check_Mouse)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in" + self.filer
        if self.ids.B_path.collide_point(*y):
            self.text = "[size=18][b]This is the Select Dataset Button[/b][/size] \n\nSelecting this button opens up the file explorer so you can select the Dataset file" + self.filer
        if self.ids.Lpath.collide_point(*y):
            self.text = "[size=18][b]This is shows the path to the current selected Dataset[/b][/size] \n\nIf no valid Dataset is selected no path is displayed" + self.filer
        if self.ids.B_nextstep.collide_point(*y):
            self.text = "[size=18][b]Load and proceed Button[/b][/size] \n\nAfter a proper dataset is selected this button will be clikable so you can load it and move on" + self.filer
        if self.ids.pipeline.collide_point(*y):
            self.text = "[size=18][b]Classification Pipeline[/b][/size] \n\nShow each of the five steps of the Classic Classification Pipeline" + self.filer

    def dismiss_popup(self):
        self._popup.dismiss()

    def Show_Load(self):
        content = LoadDialog(load=self.Load, cancel=self.dismiss_popup,start_path=os.path.dirname(os.path.abspath(__file__)))
        self._popup = Popup(title="Load file", content=content, size_hint=(0.9, 0.9),auto_dismiss=False)
        self._popup.open()

    def Load(self, path, filename):
        if filename:
            self.DataH = Data_Handler()
            self.dismiss_popup()
            if self.DataH.Load_dataset(filename[0]):
                content = Error(close=self.dismiss_popup,content = "Not Able to Load Data")
                self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
                self._popup.open()
            else:
                self.ids.Lpath.text = "...\\"+"\\".join(filename[0].split("\\")[-2:-1])+"\\"+filename[0].split("\\")[-1]
                self.ids.B_nextstep.disabled = False

    def Load_data(self):  
        self.th_EpochData = threading.Thread(target=self.DataH.Epoch_Data)
        self.th_EpochData.start()
        self.pop = Popup(title="Loading Data", content=Label(text='Loading and Epoching Data'),size_hint=(None, None), size=(400, 400), auto_dismiss=False)
        self.pop.open()
        self.clock = Clock.schedule_interval(self.Update_popup,1)
        
    def Update_popup(self,dt):
        if not self.th_EpochData.is_alive():
            self.pop.dismiss()
            self.clock.cancel()
            self.manager.current = "DataLookup"

class DataLookupScreen(Screen):
    #StringProperty definitons
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(DataLookupScreen, self).__init__(**kwargs)
        self.plotC = [[0, 0.4470, 0.7410],[0.8500, 0.3250, 0.0980],[0.9290, 0.6940, 0.1250],[0.4940, 0.1840, 0.5560],[0.4660, 0.6740, 0.1880],[0.3010, 0.7450, 0.9330],[0.6350, 0.0780, 0.1840],[0.25, 0.25, 0.25]]
        self.Base_plot = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.filer = (" \n")
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        self.helpt = ("[size=20][b]Epoching,Class selection and Data visualisation[/b][/size]\n\n"
        "[size=18][b]Data Epoching [/b][/size]\n"
        "For Time doman signals, such as a sEMG, the input is a time series of intensity values over time. Our examples use the Myo Armband as the Sensor, so we have 8 channels.\n"
        "In the Graph bellow you can see these 8 time series as an example.\n"
        "To be able to classify this signal we have to breakdown this data into windows of interest depending on what class each region represents. This process is sometimes called "
        "Epoching of the series. This process generates the diferent examples that will be used to train and test the model. "
        "The dataset was already epoched and this allows furter processing such as data selection and visual inspection\n\n"
        "[size=18][b]Class Selection [/b][/size]\n"
        "In this Screen you can chose what classes from the example dataset will be used. Its important to remember that chossing the number of classes is a tradeof. "
        "The greater the number of classes the harder it is for the classification system to diferentiate betwen them. At the same time, in most real life aplications, "
        "a greater number of classes are usualy better as it provides more generality and freedom for the system. You need to select at least two classes.\n\n"
        "Remember that you can Change the images used by swapping the images insed the Image folder for others that may better represent your contractions.")
        Window.bind(mouse_pos=self.Check_Mouse)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in" + self.filer
        if self.ids.n_ex.collide_point(*y):
            self.text = "[size=18][b]This is the Number of Epochs[/b][/size] \n\nThis value shows the number of epochs/examples the data contains" + self.filer
        if self.ids.B_classes.collide_point(*y):
            self.text = "[size=18][b]These show the avaliable Classes and their number[/b][/size] \n\nUsing these Checkbox you can select the classes that will be included" + self.filer
        if self.ids.B_rd.collide_point(*y):
            self.text = "[size=18][b]This is the Random Button[/b][/size] \n\n Clicking this button choses another random epoch as an example to be showed in the graph" + self.filer
        if self.ids.graph.collide_point(*y):
            self.text = "[size=18][b]This is the Graph[/b][/size] \n\nShould be able to indentify 8 time series in this Graph. You can see that the sEMG signals are very random and noisy" + self.filer
        if self.ids.B_pd.collide_point(*y):
            self.text = "[size=18][b]This is the Next Step Button[/b][/size] \n\nClicking in this button will confirm the selection of classes and advance to the next step" + self.filer

    def on_pre_enter(self, *args):
        self.DataH = self.manager.get_screen("Menu").DataH
        self.ids.n_ex.text = "Number of Examples found in the Dataset: " + str(int(len(self.DataH.Epoched_data)))
        [self.ids.graph.add_plot(self.Base_plot[x]) for x,_ in enumerate(self.Base_plot)] 
        self.Check_boxes= list()
        Classes = np.unique(self.DataH.Raw_Data["Class"])
        _,counts = np.unique(self.DataH.Epoched_Classes,return_counts=True)
        for x,f in enumerate(Classes):
            layout = BoxLayout(orientation="vertical")
            path = "Images\\" + str(int(f+1)) + ".PNG"
            layout.add_widget(Image(source=path,size=(100,100)))
            layout.add_widget(Label(text="Number of Examples:"+str(int(counts[x]))))
            self.Check_boxes.append(CheckBox())
            layout.add_widget(self.Check_boxes[x])
            self.ids.B_classes.add_widget(layout)
        Clock.schedule_once(lambda dt: self.Change_data())

    def Change_data(self):
        self.EMG = random.choice(self.DataH.Epoched_data)  
        self.EMG = np.array(self.EMG.drop(columns=["Subject","Class"]))
        Clock.schedule_once(lambda dt: self.Update_Graph())

    def Update_Graph(self):
        self.ids.graph.xmax = self.EMG.shape[0]
        for x in range(self.EMG.shape[1]):
            self.Base_plot[x].points = [(i, j) for i, j in enumerate(self.EMG[:,x])]

    def Select_data(self):
        classes_selected = list()
        for x in np.unique(self.DataH.Raw_Data["Class"]):
            classes_selected.append(self.Check_boxes[int(x)].active)
        if sum(classes_selected) < 2:
            self.box_popup = BoxLayout(orientation = 'horizontal')
            self.box_popup.add_widget(Label(text = "Select at Least 2 Classes"))
            self.box_popup.add_widget(Button(text = "Ok", on_press = lambda *args: self.popup_exit.dismiss(), size_hint = (0.215, 0.075)))
            self.popup_exit = Popup(title = "No enough Classes", content = self.box_popup,size_hint=(None, None), size=(400, 400), auto_dismiss = True)  
            self.popup_exit.open()
        else:   
            self.thread = threading.Thread(target=self.DataH.Select_Classes,args=(classes_selected,))
            self.thread.start()
            self.pop = Popup(title="Selecting Classes", content=Label(text='Excluding non Selected Classes... \n\n(Should not Take too long)'),size_hint=(None, None), size=(400, 400),auto_dismiss=False)
            self.pop.open()
            self.clock = Clock.schedule_interval(self.Update_popup,1)

    def Update_popup(self,dt):
        if not self.thread.is_alive():
            self.pop.dismiss()
            self.clock.cancel()
            self.manager.current = "PreProcessing"


class PreProcessingScreen(Screen):
    #StringProperty definitons
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(PreProcessingScreen, self).__init__(**kwargs)
        self.plotC = [[0, 0.4470, 0.7410],[0.8500, 0.3250, 0.0980],[0.9290, 0.6940, 0.1250],[0.4940, 0.1840, 0.5560],[0.4660, 0.6740, 0.1880],[0.3010, 0.7450, 0.9330],[0.6350, 0.0780, 0.1840],[0.25, 0.25, 0.25]]
        self.Base_plot = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.filer = (" \n")
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        self.helpt = ("[size=20][b]Data Processing[/b][/size]\n\n"
        "Compared to the other fonts of electric signal in the ambient, the sEMG signal has a very low amplitude making it hard to propely identify by simply mesuring the voltage on a single electrode.\n"
        "To combat this problem a techinique called diferential amplification is used. This this tecnique uses two electrodes and amplifies the diference bethen both signals. For sEMG, we position "
        "the two electroes apart from each other, by a few centimiters. We them assume that while both electrodes are near each other the sources of noise are suficiently further from them that "
        "the noise is equal in both. That way what is being amplified is only the signals produced by the muscles bellow the electrodes, and as the electrodes are a little apart from each other "
        "the signal each recives are displaced on time.\n\n"
        "[size=18][b]Data Processing[/b][/size]\n"
        "In this step of the pipeline the goal is further process the signal in other to magnify its usefull caracteristics and reduce its undesirable components.\nFor example, we know that the "
        "one of the most prevalecent sources of noise are power lines and charges that generate noise signal arround the 50Hz (60Hz in some countries). So by filtering the signals arround this "
        "frequency we would be decreasing the ammount of signal by a lot, while only taking a small fraction of the usefull signal. Knowing what functions and filtering techiniques works in "
        "this phase is a mixture of experience and trial and error. In this screen you are able to see what happens after appling a series of function and filters as to be able to acquire the "
        "intuition and experience necessary for this step.\n"
        "[size=18][b]Funtions and Filters avaliable[/b][/size]\n"
        "1) Absolute value: Module operation to all points (all values will become positive)\n"
        "2) Enveloping: Envelop the data using a moving average convolution window (size 20)\n"
        "3) Band Pass Filtering: Only frequencies between the two values will pass"
        "4) Band Stop Filtering: Only frequencies between the two values will be exlcluded")
        Window.bind(mouse_pos=self.Check_Mouse)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in" + self.filer
        if self.ids.B_rd.collide_point(*y):
            self.text = "[size=18][b]This is the Random Button[/b][/size] \n\nClicking this button choses another random example to be showed in the graph" + self.filer
        if self.ids.Ch_filt_bp.collide_point(*y):
            self.text = "[size=18][b]This is the Checkbox for Band Pass Filtering[/b][/size] \n\nUsing this Checkbox will only let the frequencies between" +str(self.ids.lowfreq_bp.text)+" and " +str(self.ids.highfreq_bp.text)+"." + self.filer
        if self.ids.Ch_filt_bs.collide_point(*y):
            self.text = "[size=18][b]This is the Checkbox for Band Pass Filtering[/b][/size] \n\nUsing this Checkbox will filter out the frequencies between" +str(self.ids.lowfreq_bs.text)+" and " +str(self.ids.highfreq_bs.text)+"." + self.filer
        if self.ids.lowfreq_bp.collide_point(*y):
            self.text = "[size=18][b]This is Lower Bound for the Band Pass Filter[/b][/size] \n\n Only frequencies greater than it will pass" + self.filer
        if self.ids.lowfreq_bp.collide_point(*y):
            self.text = "[size=18][b]This is Upper Bound for the Band Pass Filter[/b][/size] \n\n Only frequencies smaler than it will pass" + self.filer
        if self.ids.lowfreq_bs.collide_point(*y):
            self.text = "[size=18][b]This is Lower Bound for the Band Stop Filter[/b][/size] \n\n Only frequencies Smaller than it, and greater than the upper bound, will pass" + self.filer
        if self.ids.lowfreq_bs.collide_point(*y):
            self.text = "[size=18][b]This is Upper Bound for the Band Stop Filter[/b][/size] \n\n Only frequencies greater than it, and smaler than the lower bound, will pass" + self.filer
        if self.ids.Ch_env.collide_point(*y) or self.ids.env.collide_point(*y):
            self.text = "[size=18][b]This is the Checkbox for Enveloping[/b][/size] \n\nUsing this Checkbox will envelop the data using a moving average convolution window (size 20)" + self.filer
        if self.ids.Ch_abs.collide_point(*y) or self.ids.abs.collide_point(*y):
            self.text = "[size=18][b]This is the Checkbox for Absolute Value[/b][/size] \n\nUsing this Checkbox will apply an module operation to all points (all values will become positive)" + self.filer
        if self.ids.graph.collide_point(*y):
            self.text = "[size=18][b]This is the Graph[/b][/size] \n\nOn this Graph and example signal can be visualised" + self.filer
        if self.ids.Class_img.collide_point(*y):
            self.text = "[size=18][b]This show the Class[/b][/size] \n\nThe example on the Graph belongs to this Class" + self.filer
        if self.ids.B_fs.collide_point(*y):
            self.text = "[size=18][b]This is the Prossesing Button[/b][/size] \n\nClicking in this button will confirm the selection of processing functions and apply all to the data" + self.filer
        

    def Start_freqmove(self,text,text2,value):
        self.Clock_freq = Clock.schedule_interval(partial(self.Move_freq, text,text2, value),0.2)

    def Stop_freqmove(self):
        self.Clock_freq.cancel()

    def Move_freq(self,text,text2,value,dt):
        holder = getattr(self.ids,text)
        holder2 = getattr(self.ids,text2)
        val = int(holder.text) + int(value)
        if val < 100 and val > 0 and val != int(holder2.text):
            holder.text = str(val)
            # if self.Check_limits():
            #     holder.text = str(old)
        Clock.schedule_once(lambda dt: self.Update_Graph())


    def Check_limits(self):
        if int(self.ids.lowfreq_bp.text) == int(self.ids.highfreq_bp.text):
            return True
        if int(self.ids.lowfreq_bs.text) == int(self.ids.highfreq_bs.text):
            return True
        return False

    def on_pre_enter(self, *args):
        self.DataH = self.manager.get_screen("DataLookup").DataH
        [self.ids.graph.add_plot(self.Base_plot[x]) for x,_ in enumerate(self.Base_plot)]
        Clock.schedule_once(lambda dt: self.Change_data())

    def Change_data(self):
        self.EMG = random.choice(self.DataH.Epoched_data)
        self.ids.Class_img.source = "Images\\" + str(int(self.EMG["Class"].iloc[0]+1)) + ".PNG"
        self.EMG = np.array(self.EMG.drop(columns=["Subject","Class"]))
        Clock.schedule_once(lambda dt: self.Update_Graph())

    def Update_Graph(self):
        Plot_data = np.copy(self.EMG)
        self.ids.graph.ymax, self.ids.graph.ymin = 140, -140
        if self.ids.Ch_abs.active:
            Plot_data = np.absolute(Plot_data)
        if self.ids.Ch_filt_bp.active:
            Plot_data = self.DataH.Butter_filter(Plot_data,[int(self.ids.lowfreq_bp.text),int(self.ids.highfreq_bp.text)],'bandpass')
        if self.ids.Ch_filt_bs.active:
            Plot_data = self.DataH.Butter_filter(Plot_data,[int(self.ids.lowfreq_bs.text),int(self.ids.highfreq_bs.text)],'bandstop')
        if self.ids.Ch_env.active:
            Plot_data = self.DataH.Envelop_Data(Plot_data,100)
            self.ids.graph.ymax, self.ids.graph.ymin = 70, -70
        self.ids.graph.xmax = self.EMG.shape[0]
        for x in range(self.EMG.shape[1]):
            self.Base_plot[x].points = [(i, j) for i, j in enumerate(Plot_data[:,x])]

    def Preprosses_Final(self):
        self.thread = threading.Thread(target=self.DataH.Preprosses_Data,args=(self.ids.Ch_abs.active,self.ids.Ch_env.active,self.ids.Ch_filt_bp.active,self.ids.Ch_filt_bs.active,[int(self.ids.lowfreq_bp.text),int(self.ids.highfreq_bp.text)],[int(self.ids.lowfreq_bs.text),int(self.ids.highfreq_bs.text)]))
        self.thread.start()
        self.pop = Popup(title="Pre Prossessing", content=Label(text='Aplling Selected Pre Processing... \n\n(Should not Take too long)'),size_hint=(None, None), size=(400, 400),auto_dismiss=False)
        self.pop.open()
        self.clock = Clock.schedule_interval(self.Update_popup,1)

    def Update_popup(self,dt):
        if not self.thread.is_alive():
            self.pop.dismiss()
            self.clock.cancel()
            self.manager.current = "FeatureSelection"

class FeatureSelectionScreen(Screen):
    #StringProperty definitons
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(FeatureSelectionScreen, self).__init__(**kwargs)
        self.plotC = [[0, 0.4470, 0.7410],[0.8500, 0.3250, 0.0980],[0.9290, 0.6940, 0.1250],[0.4940, 0.1840, 0.5560],[0.4660, 0.6740, 0.1880],[0.3010, 0.7450, 0.9330],[0.6350, 0.0780, 0.1840],[0.25, 0.25, 0.25]]
        self.Base_plot0 = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.Base_plot1 = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.EMG = list((np.zeros((1000,8)),np.zeros((1000,8))))
        self.filer = (" \n")
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        self.helpt = ("[size=20][b]Feature Selection and Extraction[/b][/size]\n\n"
        "For each example gesture we have a window o variable lenght that contain 8 time series of the voltage amplitude mesured at each sensor (you can see two examples on the top rigght side). "
        "While for a humam this tipe of representation may be usefull for visual analisys, for a computational algoritm this format is not ideal to perform classification upon. This dificulty "
        "comes from various factors, such as the High number of individual points (as we have 8 series and windows may be up to 400 points) and the fact that the contraction may be located "
        "in diferent parts of the window. One of the most important parts of a classical classification pipeline is the Feature Extraction Stage. In this stage we some diferent equations "
        "to compress and extract information from each times series. A simple example would be to calculate the Mean and Variance of a signal. This two calculations are well defined and "
        "easy to perform and allow us to extract an interesting information from the signal while compressing this long time series in a single numeric value.\n"
        "[size=18][b]Feature Types[/b][/size]\n"
        "Generaly speaking Features can be divided in:\n"
        "1) Time Domain Features: These features are calculate directly from the time series\n"
        "2) Frequency Domain Features: These features are calculate after a frequency transformation is aplied and calculated from the frequency amplitue distribution\n"
        "3) Time/frequency Domain Features: These features are calculate by applying a tranformation to aquire information about the change in the frequency spectrun over time and extrating the features from it \n"
        "Each Tipe tends to have some advantages and drawbacks, usualy time domain features take less time to calculate than the other two tipes."
        "[size=18][b]Feature Selection[/b][/size]\n"
        "On this screen you have some options to select from. When the checkbox for each is ticked the progam will calculate it and plot a \"Representation\" of it the graph in the left. "
        "You will be able to see that each feature results in 8 points as it results in one result for each. "
        "[size=18][b]Features Avaliable[/b][/size]\n"
        "1) Root Mean Square: \n"
        "2) Mean Absolute Value: \n"
        "3) Slope Sing Change:\n"
        "4) Waveform Lenght:\n"
        "5) Zero Crossing:\n")
        Window.bind(mouse_pos=self.Check_Mouse)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in" + self.filer
        if self.ids.B_rd.collide_point(*y):
            self.text = "[size=18][b]This is the Random Button[/b][/size] \n\nClicking this button choses two other examples to be showed in the graphs" + self.filer
        if self.ids.graph_0.collide_point(*y) or self.ids.graph_1.collide_point(*y):
            self.text = "[size=18][b]This are both time Series Graphs[/b][/size] \n\nOn These both graphs show the examples before feature extration. The color of the border is the same as the points in the feature graph" + self.filer
        if self.ids.deuxd_graph.collide_point(*y):
            self.text = "[size=18][b]This is the Feature Graph[/b][/size] \n\nOn this graph how each of the examples are represented in each feature space. As each example has 8 sensors, there is 8 feature values for each" + self.filer
        if self.ids.Ch_rms.collide_point(*y) or self.ids.rms.collide_point(*y):
            self.text = "[size=18][b]Feature Root Mean Square [/b][/size] \n\n This feature represents a sort of intensity" + self.filer
        if self.ids.Ch_mav.collide_point(*y) or self.ids.mav.collide_point(*y):
            self.text = "[size=18][b]Feature Mean Absolute Value [/b][/size] \n\nThis feature represents the mean value of intensity (absolute)" + self.filer
        if self.ids.Ch_ssc.collide_point(*y) or self.ids.ssc.collide_point(*y):
            self.text = "[size=18][b]Feature Slope Sign Change [/b][/size] \n\nThis feature represents how much the slope changes" + self.filer
        if self.ids.Ch_wl.collide_point(*y) or self.ids.wl.collide_point(*y):
            self.text = "[size=18][b]Feature Waveform Lenght [/b][/size] \n\nThis feature calculates a sort of cumulative lenght of the signal" + self.filer
        if self.ids.Ch_zc.collide_point(*y) or self.ids.zc.collide_point(*y):
            self.text = "[size=18][b]Feature Zero Crossings[/b][/size] \n\nThis feature checks how many times the signal crosses the zero\n [color=#e41e00]WARNING: Makes NO sense if you used the absolute value filter[/color]" + self.filer
        if self.ids.B_fs.collide_point(*y):
            self.text = "[size=18][b]This is the Extract Features Button[/b][/size] \n\nClicking in this button will confirm the selection of Features to extract and apply it to all data" + self.filer

    def on_pre_enter(self, *args):
        self._scatter2d_graph = self.ids.deuxd_graph
        self.DataH = self.manager.get_screen("Menu").DataH
        self.Feats = np.zeros((2,len(self.DataH.features_all),8))
        [self.ids.graph_0.add_plot(self.Base_plot0[x]) for x,_ in enumerate(self.Base_plot0)]
        [self.ids.graph_1.add_plot(self.Base_plot1[x]) for x,_ in enumerate(self.Base_plot1)]
        Clock.schedule_once(lambda dt: self.Change_data())
      
    def Change_data(self):
        condition = True
        while condition:
            temp = random.sample(self.DataH.Epoched_data,2)
            condition = int(temp[0]["Class"].iloc[0]+1) == int(temp[1]["Class"].iloc[0]+1)
        for x in range(2):
            img = f'Class_img_{x}'
            img = getattr(self.ids,img)
            img.source = "Images\\" + str(int(temp[x]["Class"].iloc[0]+1)) + ".PNG"
            self.EMG[x] = temp[x].drop(columns=["Subject","Class"])
            self.Feats[x,] = self.DataH.Calculate_Example(pd.DataFrame(data=self.EMG[x]))
        self.scaler = MinMaxScaler(feature_range=(0, 1))
        for x in range(self.Feats.shape[1]):
            self.Feats[:,x,:] = self.scaler.fit_transform(self.Feats[:,x,:].reshape(16).reshape(-1,1)).reshape(2,8)
        Clock.schedule_once(lambda dt: self.Update_Graph())
        Clock.schedule_once(lambda dt: self.Update_Features())
    
    def Update_Graph(self):
        for x in range(2):
            gph = f'graph_{x}'
            gph = getattr(self.ids,gph)
            gph.xmax = self.EMG[x].shape[0]
            for y in range(self.EMG[x].shape[1]):
                bs = f'Base_plot{x}'
                bs = getattr(self,bs)
                bs[y].points = [(i, j) for i, j in enumerate(self.EMG[x].to_numpy()[:,y])]

    def Update_Features(self):
        self.active = [self.ids.Ch_rms.active,self.ids.Ch_mav.active,self.ids.Ch_ssc.active,self.ids.Ch_wl.active,self.ids.Ch_zc.active]
        self.DataH.Update_selected(self.active)
        self.Update_Features_Graph()
        
    def Update_Features_Graph(self):
        Plot_X = np.empty((0,2))
        Plot_Y = np.empty((0))
        label = list()
        if self.DataH.features_activated:
            j = 0  
            for x,y in enumerate(self.active):
                if y:
                    X = np.append(np.full((16,1),j+1),np.expand_dims(self.Feats[:,x,:].flatten(),axis=1),axis=1)
                    Y = np.append(np.full(8,0),np.full(8,1))
                    Plot_X = np.append(Plot_X,X,axis=0)
                    Plot_Y =  np.append(Plot_Y,Y,axis=0)
                    label.append(self.DataH.features_short[self.DataH.features_activated[j]]+" = "+str(j+1))
                    j +=1
            self._scatter2d_graph.plotScatter(Plot_X,Plot_Y,6,0,1.05,-0.05,[[0.9290, 0.6940, 0.1250],[0.4940, 0.1840, 0.5560]],", ".join(label),"Aproximate feature relations")
        else:
            self._scatter2d_graph.clear_all()

    def Extract_Features_Final(self):
        if sum(self.active) == 0:
            self.box_popup = BoxLayout(orientation = 'horizontal')
            self.box_popup.add_widget(Label(text = "Select at Least 1 Feature"))
            self.box_popup.add_widget(Button(text = "Ok", on_press = lambda *args: self.popup_exit.dismiss(), size_hint = (0.215, 0.075)))
            self.popup_exit = Popup(title = "No enough Features", content = self.box_popup,size_hint=(None, None), size=(400, 400), auto_dismiss = True)  
            self.popup_exit.open()
        else:
            self.thread = threading.Thread(target=self.DataH.Feature_Data)
            self.thread.start()
            self.pop = Popup(title="Extracting Features", content=Label(text='Calculating: 0% \n\n(May take a While)'),size_hint=(None, None), size=(400, 400),auto_dismiss=False)
            self.pop.open()
            self.clock = Clock.schedule_interval(self.Update_popup,1)

    def Update_popup(self,dt):
        p = 'Calculating: ' + str(int(100*(self.DataH.percentage[1]/self.DataH.percentage[0]))) + "% \n\n(May take a While)"
        self.pop.content = Label(text=p)
        if not self.thread.is_alive():
            self.pop.dismiss()
            self.clock.cancel()
            self.manager.current = "ClassificatorSelection"

class ClassificatorSelectionScreen(Screen):
    #StringProperty definitons
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(ClassificatorSelectionScreen,self).__init__(**kwargs)
        self.filer = (" \n")
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        self.helpt = ("[size=20][b]Classificator Selection and Visualization[/b][/size]\n\n"
        "Clearly one of the most important parts of the classification modeling is the process of selecting the apropriate model."
        "There are many models avaliable that use diferent statistics propreties to extract information and create a desigion boundary that separates diferent classes. "
        "This educational code allows for three diferent classification models. In this page you can select each and se a rough estimate of how each learns and creates the boundaries." 
        "Its important to know that the Graph is a estimate on a 2D plane not how actualy each boundary will be in the multidimentional space. To learn more about each classifier please check out the repository Wiki. \n\n"
        "It also lets you select the size of the training / testing dataset. More training usualy means better results, but may cause overfitting of the data. Also, a low number of examples on "
        "the testing dataset will not provide a very good estimation of the acuraccy of the dataset.\n\n"
        "Lastly it also lets you shuffle the order of examples and show either the training or testing points in the graph.")
        Window.bind(mouse_pos=self.Check_Mouse)
        self.Last_classifier, self.th_Classifier_final, self.Last_id, self.Last_cls = None, None, None, None
        self.Plot_tr = True

    def on_pre_enter(self, *args):
        self.DataH = self.manager.get_screen("Menu").DataH
        self.ids.TT_split.bind(value=partial(self.slider_change, self.ids.TT_split))

    def slider_change(self, s, instance, value):
        self.ids.TT_label.text = "[color=#FF0000]Train:[/color] " + str(int(value)) + "% / Test " + str(int(100-value))+ "%"
        self.ids.B_plott.disabled = True

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in" + self.filer
        if self.ids.B_lda.collide_point(*y):
            self.text = "[size=18][b]This is the Linear Discriminant Analisis (LDA) Model[/b][/size] \n\nClicking this button will train and show the rought LDA desigion boundaries" + self.filer
        if self.ids.B_svm.collide_point(*y):
            self.text = "[size=18][b]This is the Support Vector Machine (SVM) Model[/b][/size] \n\nClicking this button button will train and show the rought SVM desigion boundaries" + self.filer
        if self.ids.B_knn.collide_point(*y):
            self.text = "[size=18][b]This is the K-Nearest Neighbors (KNN) Model[/b][/size] \n\nClicking this button will train and show the rought KNN desigion boundaries" + self.filer
        if self.ids.char.collide_point(*y):
            self.text = "[size=18][b]This is the Classification Countor an examples Graph [/b][/size] \n\n Show a 2D \"Representation\" of how each Classificator learned and the points" + self.filer
        if self.ids.B_shuf.collide_point(*y):
            self.text = "[size=18][b]This is the Suffle Dataset Button[/b][/size] \n\nClicking in this button shuffles the dataset examples showing diferent possibilities depending on the data used" + self.filer
        if self.ids.B_plott.collide_point(*y):
            self.text = "[size=18][b]This is the Show / hide data Button[/b][/size] \n\nClicking in this button changes betwen showing the points used for training and the ones reserved for testing" + self.filer
        if self.ids.TT_split.collide_point(*y) or self.ids.TT_label.collide_point(*y):
            self.text = "[size=18][b]This is the Slider for the Train / Test Split[/b][/size] \n\n Moving this Slider controns the percentage of data used for training and reserved for testing" + self.filer
        if self.ids.B_fs.collide_point(*y):
            self.text = "[size=18][b]This is the Select Classifier Button[/b][/size] \n\nClicking in this button will confirm the model selection and fully train it" + self.filer

    def Shuffle_data(self):
        self.perm = np.random.permutation(self.DataH.Featured_Data.shape[1])
        self.DataH.Featured_Data = self.DataH.Featured_Data[:,self.perm]
        self.ids.char.clear_plot()
        self.ids.char.Plot_clear()
        self.ids.B_lda.text, self.ids.B_svm.text, self.ids.B_knn.text = "Linear Discriminant Analysis", "Support Vector Machines", "K Nearest Neighbors"

    def Change_ploted_points(self):
        self.Plot_tr = not self.Plot_tr
        if self.Plot_tr:
            self.ids.B_plott.text = "[color=#52ef00]Show Training points[/color] / Show Test Points"
        else:
            self.ids.B_plott.text = "Show Training points / [color=#52ef00]Show Test Points[/color]"
        self.ids.char.Change_plot(self.Plot_tr)

    def Classifier_start(self,id,cls):
        self.Last_id, self.Last_cls = id,cls
        self.ids.B_plott.disabled, self.ids.B_shuf.disabled = False, False
        self.ids.B_lda.text, self.ids.B_svm.text, self.ids.B_knn.text = "Linear Discriminant Analysis", "Support Vector Machines", "K Nearest Neighbors"
        c = getattr(self.ids,id)
        c.text = "[color=#52ef00]"+ c.text + "[/color]"
        self.split = round(self.DataH.Featured_Data.shape[1] * (self.ids.TT_split.value*0.01))
        X = self.DataH.Featured_Data[2:,:].T
        Y = self.DataH.Featured_Data[0,:]

        self.Last_classifier = getattr(self,cls)
        self.th_Classifier = threading.Thread(target=self.Prepare_data,args=(X,Y,self.Last_classifier,self.split))
        self.th_Classifier.start()
        self.pop = Popup(title="Training Classifier", content=Label(text="Traing the Classifier\n(may take a while depending on the classifier)"),size_hint=(None, None), size=(400, 400),auto_dismiss=False)
        self.pop.open()
        self.clock = Clock.schedule_interval(lambda dt: self.Update_popup(),1)
    
    def Prepare_data(self,X,Y,Fclf,split):
        self.ids.char.clear_plot()
        pca = PCA(n_components=2)
        Xreduced = pca.fit_transform(X)
        self.X_train, self.Y_train, self.X_test, self.Y_test = Xreduced[0:split,:], Y[0:split], Xreduced[split:,:], Y[split:]
        Fclf(self.X_train, self.Y_train)
        self.ids.char.Plot_Contour(self.X_train, self.X_test, self.clf)
        self.ids.char.Plot_Scatter(self.X_train,self.Y_train,self.X_test,self.Y_test,self.Plot_tr)
      
    def LDA_training(self,X,Y):
        lda = LDA()
        self.clf = lda.fit(X,Y)

    def SVM_training(self,X,Y):
        svm = SVC(kernel='rbf',gamma='scale')
        self.clf = svm.fit(X,Y)

    def KNN_training(self,X,Y):
        knn = KNeighborsClassifier(3)
        self.clf = knn.fit(X,Y)

    def Select_Classifier_Final(self):
        if self.Last_classifier is None:
            self.box_popup = BoxLayout(orientation = 'horizontal')
            self.box_popup.add_widget(Label(text ="Before Proceding you need to chose a Classifier"))
            self.box_popup.add_widget(Button(text = "Ok", on_press = lambda *args: self.popup_exit.dismiss(), size_hint = (0.215, 0.075)))
            self.popup_exit = Popup(title = "Select a Classifier", content = self.box_popup,size_hint=(None, None), size=(400, 400), auto_dismiss = True)  
            self.popup_exit.open()
        else:
            X = self.DataH.Featured_Data[2:,:].T
            Y = self.DataH.Featured_Data[0,:]
            self.X_train, self.Y_train, self.X_test, self.Y_test = X[0:self.split,:], Y[0:self.split], X[self.split:,:], Y[self.split:]
            self.th_Classifier_final = threading.Thread(target=self.Last_classifier,args=(self.X_train,self.Y_train))
            self.th_Classifier_final.start()
            self.pop = Popup(title="Training Classifier", content=Label(text="Traing the Final Classifier\n(may take a while depending on the classifier)"),size_hint=(None, None), size=(400, 400),auto_dismiss=False)
            self.pop.open()
            self.clock = Clock.schedule_interval(lambda dt: self.Update_popup(),1)

    def Update_popup(self):
        if not self.th_Classifier.is_alive():
            self.pop.dismiss()
            self.clock.cancel()
        if self.th_Classifier_final is not None:
            if  not self.th_Classifier_final.is_alive():
                self.manager.current = "Classification"

class ClassificationScreen(Screen):
    #StringProperty definitons
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(ClassificationScreen,self).__init__(**kwargs)
        self.plotC = [[0, 0.4470, 0.7410],[0.8500, 0.3250, 0.0980],[0.9290, 0.6940, 0.1250],[0.4940, 0.1840, 0.5560],[0.4660, 0.6740, 0.1880],[0.3010, 0.7450, 0.9330],[0.6350, 0.0780, 0.1840],[0.25, 0.25, 0.25]]
        self.Base_plot0 = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.Base_plot1 = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.Base_plot2 = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.EMG = list((np.zeros((1000,8)),np.zeros((1000,8)),np.zeros((1000,8))))
        self.filer = (" \n")
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        self.helpt = ("[size=20][b]Real time Model Testing[/b][/size]\n\n"
        "After the model is trained with the test dataset we can start to use the model to test the never seen before data. "
        "On this page you can try a little and select one of three random examples for the model to classify and see if it got it right or not.\n\n "
        "This process may look meaningless, however it may help you learn the visual diferences between classes and that may drive inspiration for future improvements to the model.\n\n"
        "When you are ready to proced the next page will test all examples and generate a final report for the model performance.")
        Window.bind(mouse_pos=self.Check_Mouse)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in" + self.filer
        if self.ids.graph_0.collide_point(*y) or self.ids.graph_1.collide_point(*y) or self.ids.graph_2.collide_point(*y):
            self.text = "[size=18][b]This is the Example Testing Gestures[/b][/size] \n\nThese Graphs show some random examples for you to chose betwen and classify" + self.filer
        if self.ids.ex_0.collide_point(*y) or self.ids.ex_1.collide_point(*y) or self.ids.ex_2.collide_point(*y):
            self.text = "[size=18][b]This is the Example Testing Buttons[/b][/size] \n\nClicking in this buttom will select this example an classify it" + self.filer
        if self.ids.Pred_IMG.collide_point(*y):
            self.text = "[size=18][b]This is the Predicted Gesture[/b][/size] \n\nThe result predicted by the classifier for the example chosen" + self.filer
        if self.ids.True_IMG.collide_point(*y):
            self.text = "[size=18][b]This is the True Gesture[/b][/size] \n\nThe True label of the example chosen" + self.filer
        if self.ids.B_fs.collide_point(*y):
            self.text = "[size=18][b]This is test all Buttom[/b][/size] \n\nClicking wil test all examples and lead you to the results screen" + self.filer

    def on_pre_enter(self, *args):
        self.DataH = self.manager.get_screen("Menu").DataH
        self.clf= self.manager.get_screen("ClassificatorSelection").clf
        self.X_train, self.Y_train =  self.manager.get_screen("ClassificatorSelection").X_train, self.manager.get_screen("ClassificatorSelection").Y_train
        self.X_test, self.Y_test = self.manager.get_screen("ClassificatorSelection").X_test, self.manager.get_screen("ClassificatorSelection").Y_test
        self.EMG_train,self.Class_train = self.DataH.Epoched_data[0:self.X_train.shape[0]], self.DataH.Epoched_data[0:self.X_train.shape[0]]
        self.EMG_test, self.Class_test = self.DataH.Epoched_data[self.X_train.shape[0]:], self.DataH.Epoched_data[self.X_train.shape[0]:]
        [self.ids.graph_0.add_plot(self.Base_plot0[x]) for x,_ in enumerate(self.Base_plot0)]
        [self.ids.graph_1.add_plot(self.Base_plot1[x]) for x,_ in enumerate(self.Base_plot1)]
        [self.ids.graph_2.add_plot(self.Base_plot2[x]) for x,_ in enumerate(self.Base_plot2)]
        Clock.schedule_once(lambda dt: self.Change_data())
    
    def Change_data(self):
        self.Examples = np.random.permutation(self.X_test.shape[0])[0:3]
        for x,y in enumerate(self.Examples):
            self.EMG[x] = self.EMG_test[y].drop(columns=["Subject","Class"]).to_numpy()
        Clock.schedule_once(lambda dt: self.Update_Graph())
    
    def Update_Graph(self):
        for x in range(3):
            gph = f'graph_{x}'
            gph = getattr(self.ids,gph)
            gph.xmax = self.EMG[x].shape[0]
            for y in range(self.EMG[x].shape[1]):
                bs = f'Base_plot{x}'
                bs = getattr(self,bs)
                bs[y].points = [(i, j) for i, j in enumerate(self.EMG[x][:,y])]

    def Test_example(self,id):
        true = self.Y_test[self.Examples[id]]
        pred = self.clf.predict(self.X_test[self.Examples[id],:].reshape(1,-1))[0]
        self.ids.Pred_IMG.source = "Images\\" + str(int(pred+1)) + ".PNG"
        self.ids.True_IMG.source = "Images\\" + str(int(true+1)) + ".PNG"
        Clock.schedule_once(lambda dt: self.Change_data())

    def Result_Screen(self):
        self.th_Evaluation = threading.Thread(target=self.Testing_Evaluation)
        self.th_Evaluation.start()
        self.pop = Popup(title="Predicting test Data", content=Label(text="Predcting Test data and calculating Evaluation. \n(may take a while depending on the classifier)"),size_hint=(None, None), size=(400, 400),auto_dismiss=False)
        self.pop.open()
        self.clock = Clock.schedule_interval(lambda dt: self.Update_popup(),1)

    def Testing_Evaluation(self):
        self.pred = self.clf.predict(self.X_test)

    def Update_popup(self):
        if  not self.th_Evaluation.is_alive():
            self.pop.dismiss()
            self.clock.cancel()
            self.manager.current = "Results"

class ResultsScreen(Screen):
    #StringProperty definitons
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(ResultsScreen,self).__init__(**kwargs)
        self.filer = (" \n")
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        self.helpt = ("[size=20][b]Results and Measures of Performance[/b][/size]\n\n"
        "This page shows you two tabels generated after testing all examples on the Testing dataset.\n\n"
        "[b]Confusion Matrix[/b]\n"
        "The graph on the right is called the Confusion Matrix. It shows the True Positives, True Negatives, False Positives and False negatives.\n\n"
        "[b] Performance Metrics[/b]\n"
        "The graph on the left shows the f1-score, precision and recall for each class, and for the macro and weighted average of classes.")
        Window.bind(mouse_pos=self.Check_Mouse)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in" + self.filer
        if self.ids.results.collide_point(*y):
            self.text = "[size=18][b]This is the Results Graph[/b][/size] \n\nThis shows the results and performance metrics " + self.filer
        if self.ids.B_save.collide_point(*y):
            self.text = "[size=18][b]This is Save Button[/b][/size] \n\nThis Saves the Resuls as a image on the root folder of the program" + self.filer
        if self.ids.B_exit.collide_point(*y):
            self.text = "[size=18][b]This is end Button[/b][/size] \n\nThis ends the program" + self.filer

    def on_pre_enter(self, *args):
        self.DataH = self.manager.get_screen("Menu").DataH
        self.clf= self.manager.get_screen("ClassificatorSelection").clf
        self.X_train, self.Y_train =  self.manager.get_screen("ClassificatorSelection").X_train, self.manager.get_screen("ClassificatorSelection").Y_train
        self.X_test, self.Y_test = self.manager.get_screen("ClassificatorSelection").X_test, self.manager.get_screen("ClassificatorSelection").Y_test
        predictions = self.clf.predict(self.X_test)
        Clock.schedule_once(lambda dt: self.Plot_Suft(),0.5)

    def Plot_Suft(self):
        predictions = self.clf.predict(self.X_test)
        self.ids.results.Present_results(predictions,self.Y_test,self.clf)

    def Save_plot(self):
        self.ids.results.Save_fig("Results.png")


#Support Classes for Matplotlib Graphs and Plots   
class Chart(BoxLayout): 
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fig, self.ax1 = plt.subplots()
        self.ax1.scatter([], [],)
        self.mpl_canvas = self.fig.canvas
        self.add_widget(self.mpl_canvas)

    def Plot_Contour(self,X_train,X_test,clf):
        self.ax1.clear()
        rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.tab10.colors)
        xx, yy = self.make_meshgrid(np.concatenate((X_train[:,0],X_test[:,0]),axis=0), np.concatenate((X_train[:,1],X_test[:,1]),axis=0))
        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        self.ax1.contourf(xx, yy, Z, alpha=0.8)

    def Plot_Scatter(self,X_train, Y_train,X_test,Y_test,hide_te):
        rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.tab10.colors)
        self.trsca = self.ax1.scatter(X_train[:,0], X_train[:,1], c=Y_train, edgecolors='k')
        rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.tab10.colors)
        self.tesca = self.ax1.scatter(X_test[:,0], X_test[:,1], c=Y_test, edgecolors='k')
        self.Change_plot(hide_te)

    def Change_plot(self,hide_te):
        if hide_te:
            self.tesca.set_visible(False)
            self.trsca.set_visible(True)
        else:
            self.tesca.set_visible(True)
            self.trsca.set_visible(False)
        self.mpl_canvas.draw_idle()

    def make_meshgrid(self,x, y, h=.02):
        x_min, x_max = x.min() - 1, x.max() + 1
        y_min, y_max = y.min() - 1, y.max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
        return xx, yy
    
    def clear_plot(self):
        self.ax1.clear()

    def Plot_clear(self):
        self.ax1.scatter([], [])
    
class Results(BoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fig, (self.ax1,self.ax2) = plt.subplots(1,2)
        # self.fig, self.ax1 = plt.subplots()
        self.mpl_canvas = self.fig.canvas
        self.add_widget(self.mpl_canvas) 

    def Present_results(self,pred,ground,clf):
        cm = confusion_matrix(ground,pred)
        columns = ['class %s' %(i) for i in range(len(np.unique(ground)))]
        df_cm = pd.DataFrame(cm, index=columns, columns=columns)
        # sn.set(font_scale=1.4) # for label size
        sn.heatmap(df_cm, annot=True,cmap='Oranges', annot_kws={"size": 16},ax=self.ax1)
        clf_report = classification_report(ground, pred, target_names=columns, output_dict=True)
        sn.heatmap(pd.DataFrame(clf_report).iloc[:-1, :].T, annot=True,ax=self.ax2)
        self.mpl_canvas.draw_idle()

    def Save_fig(self,path):
        self.fig.savefig(path)

#Basic Necessary Classes, Managers, functions and main call
class Manager(ScreenManager):
    pass

class Error(FloatLayout):
    close = kyprops.ObjectProperty(None)
    content = kyprops.StringProperty(None)

class LoadDialog(FloatLayout):
    load = kyprops.ObjectProperty(None)
    cancel = kyprops.ObjectProperty(None)
    start_path = kyprops.StringProperty(None)

# kv = Builder.load_file("MyoEletricControler.kv")
class MyoEletricControlerApp(App):
    def build(self):
        Window.size = (1280,720)
        m = Manager()
        return m


def Envelop_Data(EMG,W):
    box = np.ones((W,))/W
    conv =  np.array([np.convolve(EMG[:,x], box, mode='same') for x in range(EMG.shape[1])])
    return conv.T

if __name__ == '__main__':
    MyoEletricControlerApp().run()
