/**
* EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
* Arduino Controler
* Made for Ardunio BLE 33
* Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
* <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
* Licensed under the MIT License. See License.txt in root.
* 
* Based on ArduinoSoftware_Arduino_IDE
*  Copyright 2016 by Tim Duente <tim.duente@hci.uni-hannover.de>
*  Copyright 2016 by Max Pfeiffer <max.pfeiffer@hci.uni-hannover.de>
*  <https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home>
*  Licensed under "The MIT License (MIT) 'military use of this product is forbidden' V 0.2"
*/

/*
 * EMSSystem.h
 *
 *  Created on: 26.05.2014
 *      Author: Tim Duente
 */

#ifndef EMSSYSTEM_H_
#define EMSSYSTEM_H_

#include "EMSChannel.h"
#include "Debug.h"

#define ACTION 'G'
#define CHANNEL 'C'
#define INTENSITY 'I'
#define TIME 'T'
#define OPTION 'O'

class EMSSystem {
public:
	EMSSystem(uint8_t channels);
	virtual ~EMSSystem();

	virtual void addChannelToSystem(EMSChannel *emsChannel);
	virtual void doCommand(String *command);
	void shutDown();
	virtual uint8_t check();
	static void start();

protected:
	virtual void doActionCommand(String *command);
	virtual void setOption(String *option);
	virtual bool getChannelAndValue(String *option, int *channel, int *value);
	virtual int getNextNumberOfString(String *command, uint8_t startIndex);

private:
	EMSChannel **emsChannels;
	uint8_t maximum_channel_count;
	uint8_t current_channel_count;
	bool isInRange(int channel);
};

#endif /* EMSSYSTEM_H_ */
