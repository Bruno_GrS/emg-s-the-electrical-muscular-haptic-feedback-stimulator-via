#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Machine Learning Controlers - Convolution Neural Network Gesture Controler
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

import numpy as np
import os
import pandas as pd
from collections import deque
from sklearn.preprocessing import StandardScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.svm import SVC
import cv2


#Keras
from keras import optimizers
from keras.models import Sequential 
from keras.layers import GlobalAveragePooling2D, Conv2D, MaxPooling2D, SpatialDropout2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K
from keras.utils import to_categorical, Sequence, normalize
from keras.callbacks import History


class DataHandler():
    def __init__(self,**kwargs):
        self.prepross_all = ["Absolute Value","Envelop Data"]
        self.features_activated = list()
        self.prepross_activated = list()
        self.dict_funcs = {"Absolute Value": self.Absolute_Value, "Envelop Data": self.Envelop_Data}                                   
        self.Dataset_Loaded = False
        self.percentage = [0,1]
    
    def Load_dataset(self,path):
        try:
            self.Raw_Data = pd.read_csv(path,index_col=0)
            if  "Class" in self.Raw_Data.columns:
                self.Raw_Data["Class"] -= 1 # arruma para começar em 0
                self.Raw_Data = self.Raw_Data.iloc[0:50000] #For Test Purposes-------------
                self.Dataset_Loaded = True
                return not self.Dataset_Loaded
            else:
                return not self.Dataset_Loaded
        except:
            return not self.Dataset_Loaded

    def Epoch_Data(self):
        self.Epoched_data = list()
        count = 0
        for x in range(self.Raw_Data.shape[0]):
            
            if self.Raw_Data["Class"][count] != self.Raw_Data["Class"][x]:
                self.Epoched_data.append(self.Raw_Data.loc[count:x-1].copy())
                count = x

    def Select_Classes(self,classes_selected):
        temp = list()
        for x,data in enumerate(self.Epoched_data):
            if classes_selected[int(data["Class"].iloc[0])]:
                temp.append(self.Epoched_data[x])
        self.Epoched_data = temp
 
    def Preprosses_Data(self):
        for x,data in enumerate(self.Epoched_data):
            data = np.array(data.drop(columns=["Subject","Class"]))
            for ch in range(data.shape[1]):
                for elem in self.prepross_activated:
                    data[:,ch] = self.Call_funcs(elem,data[:,ch])
            (self.Epoched_data[x]).loc[:,'emg0':'emg7'] = data

    def Window_Scale(self):
        self.X_data = np.zeros((len(self.Epoched_data),8,140,3))
        self.Y_data = np.zeros((len(self.Epoched_data),1),dtype=int)
        for x in range(len(self.Epoched_data)):
             self.X_data[x,] = cv2.resize((self.Epoched_data[x]).loc[:,'emg0':'emg7'].to_numpy(dtype='uint8'),(8,420)).reshape(8,140,3)
             self.Y_data[x] = (self.Epoched_data[x]).loc[:,"Class"].iloc[1]
        
    def Train_Classifier(self,B_size,N_epoch):
        self.Model = self.Create_Model(self.X_data[0,],np.unique(self.Y_data).shape[0])
        self.Y_train = to_categorical(self.Y_data,num_classes=np.unique(self.Y_data).shape[0])
        self.Model.fit(self.X_data,self.Y_train,
                        batch_size=B_size,
                        epochs=N_epoch)

    #======== Test Functions ============

    def Window_train(self,X):
        return cv2.resize(X,(8,420)).reshape(8,140,3)

    def Preprosses_Test(self,data):
        for ch in range(data.shape[0]):
                for elem in self.prepross_activated:
                    data[ch,:] = self.Call_funcs(elem,data[ch,:])
        return data            


    def Test_Classifier(self,data):
        predIdxs = self.Model.predict(data, verbose=0)
        return np.argmax(predIdxs, axis=1)
        

    #======== Suport Functions ============

    def Call_funcs(self,elem,data):
        """Calls Feature functions based on dictionary reading""" 
        func_to_call = self.dict_funcs[elem]
        result = func_to_call(data)
        return result

        
    def Update_All_Selections(self,Preact):
        """Update selected Preprocessing steps and Features selected"""
        self.prepross_activated.clear()
        for x,y in enumerate(self.prepross_all):
            if Preact[x]:
                self.prepross_activated.append(y)        

    def Absolute_Value(self,data):
        """
        Calculate the absolute value of a array

        :data: array of values -- np.array 1D
        :return: array of values -- np.array 1D
        """
        np.absolute(data)
        return data

    def Envelop_Data(self,series):
        """
        Envelops a time series via convolution, maintaining length

        :series: Time series -- np.array 1D
        :return: Enveloped Time series -- np.array 1D, same length as series
        """
        W = 100
        box = np.ones((W,))/W
        conv =  np.convolve(series, box, mode='same')
        return conv.T       

    def Create_Model(self,X,Y):
        model = Sequential()
        model.add(Conv2D(64, (3, 3), activation='relu', padding='same', input_shape=X.shape))
        model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
        model.add(MaxPooling2D(2))
        model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
        model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
        model.add(MaxPooling2D(2))   
        model.add(GlobalAveragePooling2D())
        model.add(Dense(64, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(Y,activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        model.summary()
        return model