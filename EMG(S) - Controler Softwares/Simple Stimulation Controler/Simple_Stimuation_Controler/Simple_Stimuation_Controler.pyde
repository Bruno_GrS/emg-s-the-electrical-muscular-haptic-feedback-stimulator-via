#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Basic Controler and Tester for the EMS Estimulator Arduino Board
# Made for Processing 3.5.4
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

add_library('serial')
add_library('controlP5')

#setup class so we can use stuff outiside the setup function
class Setup:
    def __init__(self):
        self.Coms = "No Connection"
    def Serial_start(self,port, baudrate):
        if Serial.list() > 1:
            self.Port = Serial(this, Serial.list()[port], baudrate)
            self.Coms = ""
        else:
            self.Port = None
    def Serial_comunicate(self):
        if self.Port is not None and self.Port.available() > 0:
            self.Coms = self.Port.readString()
    def Font(self, name, Size):
        self.Font1 = createFont(name, Size)
        textFont(self.Font1)
    def Layout_display(self):
        fill(0)
        textSize(25)
        text("Comand Controler",280,50)
        text("Comands",300,400)
        text("Serial Answers",280,540)
        stroke(0)
        strokeWeight(2)
        line(280,55,280+textWidth("Comand Controler"),55)
        line(300,405,300+textWidth("Comands"),405)
        line(280,545,280+textWidth("Serial Answers"),545)
        stroke(255)
        strokeWeight(5)
        line(20,360,780,360)
        line(20,500,780,500)
        strokeWeight(2)
    def CP5(self):
        self.cp5 = ControlP5(this)
    def B_onoff(self):
        self.on_off = Selector(100,100,160,color(14,27,64),color(67,104,220),16,"ON","OFF","ON/OFF:")
    def B_intensity(self):
        self.Intensity = Intensity_controler(200, 200, 399, 16, 1, "Intensity:")
    def B_Tslider(self):
        self.Time_slider = T_slider(200, 250, 300, 10, 1, 5, 5,"Time:")
    def B_send(self):
        self.Bsend = Send_button(500,300,50,16, color(69,167,53), color(236,15,36),"Send Comand")
c = Setup()

#Classes for button sliders etc
class T_slider():
    def __init__(self,x,y,len,width,MinR,MaxR,Nticks,label):
        self.x = x
        self.y = y
        self.label = label
        c.cp5.addSlider(" ").setPosition(x, y).setSize(len, width).setRange(MinR, MaxR).setNumberOfTickMarks(Nticks) 
    def display(self):
        textSize(16)
        text(self.label, self.x-100, self.y+10)
        
class Button:
    def __init__(self,x, y, width , background, border, size, label):
        self.x = x
        self.y = y
        self.background = background
        self.label = label
        self.border = border
        self.size = size
        self.width = width
        
    def display(self):
        fill(self.background)
        stroke(self.border)
        textAlign(LEFT)
        textSize(self.size)
        rect(self.x, self.y, textWidth(self.label) + self.width, (textAscent()+textDescent()) + 20, 7)
        fill(255)
        text(self.label, self.x + self.width/2, self.y + 28)
        
    def over(self):
        return mouseX > self.x and mouseX < self.x+textWidth(self.label)+self.width and mouseY > self.y and mouseY < self.y+textAscent()+textDescent() + 20

class Selector:
    def __init__(self, x, y, width, C_on, C_off, size, label1, label2, text):
        self.C_on = C_on
        self.C_off = C_off;
        self.Text = text;
        self.x = x;
        self.y = y;
        self.size = size;
        self.width = width;
        self.button1 = Button(x+100,y,width,C_off,C_off,size,label1);
        self.button2 = Button(x+300,y,width,C_on,C_on,size,label2);
    
    def clikable(self):
        return self.button1.over() and self.button1.background == self.C_off or self.button2.over() and self.button2.background == self.C_off

    def cliked(self):
        self.update()
  
    def update(self):
        temp = self.button1.background;
        self.button1.background = self.button2.background;
        self.button1.border = self.button2.background;
        self.button2.background = temp;
        self.button2.border = temp;
  
    def display(self):
        textSize(self.size);
        fill(0);
        textAlign(LEFT);
        text(self.Text,self.x,self.y+28);
        self.button1.display();
        self.button2.display();

class Send_button:
    def __init__(self, x, y, Bwidth, size, C_on, C_off, label):
        self.label = label
        self.C_on = C_on
        self.C_off = C_off
        self.button1 = Button(x, y, Bwidth, C_off, C_off, size, label)
        self.last_t = str(c.cp5.get(" ").getValue())
        self.last_i = c.cp5.get("").getText()
        self.Comand_sent = ""
        
    def cliked(self):
        if c.on_off.button1.background == c.on_off.C_on:
            if c.Port is not None:
                self.button1.background = self.C_on
                self.button1.border = self.C_on
                comand = "I" + c.cp5.get("").getText() + "T" + str(int(c.cp5.get(" ").getValue()*1000)) + "G\n"
                c.Port.write("C0" + comand)
                c.Port.write("C1" + comand)
                self.Comand_sent = "C0" + comand + "\n" + "C1"+ comand
    def update(self):
        temp1 = c.cp5.get("").getText()
        temp2 = str(c.cp5.get(" ").getValue())
        if temp1 != self.last_i:
            self.button1.background = self.button1.border = c.Bsend.C_off
        if temp2 != self.last_t:
            self.button1.background = self.button1.border = c.Bsend.C_off
        if c.on_off.button1.background != c.on_off.C_on:
            self.button1.background = self.button1.border = c.Bsend.C_off
        self.last_i = temp1
        self.last_t = temp2                
        
    def display(self):
        self.button1.display()
        textSize(20)
        fill(0)
        text("Comand Sent:",100,450)
        fill(255)
        text(self.Comand_sent,280,450)
    
class Intensity_controler:
    def __init__(self, xp, yp, sw, sh, l, Text):
        self.Text = Text
        self.Scrollint = HScrollbar(xp, yp, sw, sh, l)
        c.cp5.addTextfield("").setPosition(xp+420, yp-21).setSize(40, 40).setFont(c.Font1).setColorBackground(color(209,209,209)).setColorForeground(color(209,209,209))
        c.cp5.get("").setValue("0")
        c.cp5.get("").setInputFilter(ControlP5.INTEGER)
            
    def update(self):
        self.Scrollint.update()
        
    def display(self):
        self.Scrollint.display()
        fill(0)
        textAlign(LEFT)
        text(self.Text, self.Scrollint.xpos-100, self.Scrollint.ypos+12)
    

class HScrollbar:
    def __init__(self,xp, yp, sw, sh, l):
        self.swidth = sw
        self.sheight = sh
        self.widthtoheight = sw - sh
        self.ratio = float(sw) / float(self.widthtoheight)
        self.xpos = xp
        self.ypos = yp-self.sheight/2
        self.spos = self.xpos + self.swidth - self.sheight
        self.newspos = self.spos
        self.sposMin = self.xpos
        self.sposMax = self.xpos + self.swidth - self.sheight
        self.loose = l
        self.field = 0
    
    def update(self):
        if self.overEvent():
            self.over = True
        else:
            self.over = False
        if mousePressed and self.over:
            self.locked = True
        if not mousePressed:
            self.locked = False
        if self.locked:
            self.newspos = self.constrain(mouseX-self.sheight/2, self.sposMin, self.sposMax)
        if c.cp5.get("").getText() == "":
            self.field = 0
        elif int(c.cp5.get("").getText()) > 100:
            c.cp5.get("").setValue("100")
        else:
            self.field = int(constrain(float(c.cp5.get("").getText()),0.0,100.0))    
        if not self.locked and self.field != int(self.getPos()):
            self.spos = (3.8 * (self.field))+200
            self.newspos = self.spos    
        if abs(self.newspos - self.spos) > 1:
            self.spos = self.spos + (self.newspos-self.spos)/self.loose
            c.cp5.get("").setValue(str(int(self.getPos())))
        
    def constrain(self, val, minv, maxv):
        return min(max(val, minv), maxv)
    
    def overEvent(self):
        if mouseX > self.xpos and mouseX < self.xpos+self.swidth and mouseY > self.ypos and mouseY < self.ypos+self.sheight:
            return True
        else:
            return False
    
    def display(self):
        noStroke()
        fill(255)
        rect(self.xpos, self.ypos, self.swidth, self.sheight,7)
        if self.over or self.locked:
            fill(0, 0, 0)
        else:
            fill(102, 102, 102)
        rect(self.spos, self.ypos, self.sheight, self.sheight,7)
    
    def getPos(self):
        return (self.spos-200)/3.8

  
def setup():
    
    #Window
    size(800,700)
    background(color(209,209,209))
    frameRate(30)
    #Serial
    conected =  c.Serial_start(0,115200)
    if Serial.list()  > 1:
        print ("Connected to ", conected)
    #Fonts
    c.Font("Arial Rounded MT Bold",16) 
      
    #ControlP5
    c.CP5()
    #Buttons
    c.B_onoff()
    c.B_intensity()
    c.B_Tslider() 
    c.B_send()  
     
def draw():
    background(color(209,209,209))
    #Serial check
    c.Serial_comunicate()
    fill(0)
    text("Serial Answer:",100,600)
    fill(255)
    textSize(10)
    text(c.Coms,280,580)
    #Layout Update
    c.Layout_display()    
    #Buttons display and update    
    c.on_off.display()
    c.Intensity.update()
    c.Intensity.display()
    c.Time_slider.display()
    c.Bsend.update()
    c.Bsend.display()
         
#Mouse functions

def mousePressed():
    if c.on_off.clikable():
        c.on_off.cliked()  
    if c.Bsend.button1.over():
        c.Bsend.cliked()

def mouseMoved():
    if c.on_off.button1.over() and c.on_off.button1.background != c.on_off.C_on:
        c.on_off.button1.border = 255
    else:
        c.on_off.button1.border = c.on_off.button1.background
        
    if c.on_off.button2.over() and c.on_off.button2.background != c.on_off.C_on:
        c.on_off.button2.border = 255
    else:
        c.on_off.button2.border = c.on_off.button2.background
    if c.Bsend.button1.over():
        c.Bsend.button1.border = 255
    else:
        c.Bsend.button1.border = c.Bsend.button1.background    
