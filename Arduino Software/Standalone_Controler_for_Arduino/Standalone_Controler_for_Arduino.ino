/**
* EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
* Arduino Standalone Controler
* Made for Ardunio BLE 33
* Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
* <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
* Licensed under the MIT License. See License.txt in root.
*
* Based on ArduinoSoftware_Arduino_IDE
*  Copyright 2016 by Tim Duente <tim.duente@hci.uni-hannover.de>
*  Copyright 2016 by Max Pfeiffer <max.pfeiffer@hci.uni-hannover.de>
*  <https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home>
*  Licensed under "The MIT License (MIT) 'military use of this product is forbidden' V 0.2"
*/

/**
 * Minor Revisions to this file by Pedro Lopes <plopesresearch@gmail.com>, all credit remains with the original authors (see above).
 */

// Necessary Imports
#include <ArduinoBLE.h>
#include "ArduinoSoftware.h"
#include "Wire.h"
#include "AD5252.h"
#include "EMSSystem.h"
#include "EMSChannel.h"
#include "Debug.h"
#include "BLEModule.h"
#include <cstdlib>


// Initialization of control objects for EMS
AD5252 digitalPot(0);
EMSChannel emsChannel1(5, 4, A2, &digitalPot, 1);
EMSChannel emsChannel2(6, 7, A3, &digitalPot, 3);
EMSSystem emsSystem(2);
// Initialization of control objects for BLE
static String Myo_Name = "My Myo";
int BLE_CONNECTED = 1;
int THRESH = 100;
BLEService    ControlService;
BLECharacteristic    CommandCharacteristic;
BLEService    EMGService;
BLECharacteristic    EMG0Characteristic;
BLECharacteristic    EMG1Characteristic;
BLECharacteristic    EMG2Characteristic;
BLECharacteristic    EMG3Characteristic;
BLEDevice peripheral;

String command = "";

void setup() {

  // Start the BLE service and start scanning for the Myo
  BLE.begin();
  BLE.scanForUuid(Desired_UUID);

	//Add the EMS channels and start the control
	emsSystem.addChannelToSystem(&emsChannel1);
	emsSystem.addChannelToSystem(&emsChannel2);
	EMSSystem::start();
	emsSystem.shutDown();
	command.reserve(21);
}

//String hexCommandString;
//const String BTLE_DISCONNECT = "Connection End";

void loop() {
  	//Checks whether a signal has to be stoped
  	if (emsSystem.check() > 0) {
      
  	}
   
    // Check with the BLE is connected
    if (BLE_CONNECTED){  
        //Check if a peripheral has been discovered
        BLEDevice peripheral = BLE.available();
    
        if (peripheral) {
            //Confirm that the correct Myo was found
            if (peripheral.localName() != Myo_Name) {
                return;
            }
            
            //Stop Scanning 
            BLE.stopScan();
            
            //Connect to Myo
            if (peripheral.connect()){
                BLE_CONNECTED = 0;
            }
            
            //Discover peripheral attributes
            if (peripheral.discoverAttributes()) {
                //Define de services and characteristics
                ControlService = peripheral.service("d5060001-a904-deb9-4748-2c7f4a124842");
                CommandCharacteristic = peripheral.characteristic("d5060401-a904-deb9-4748-2c7f4a124842");
                EMGService = peripheral.service("d5060005-a904-deb9-4748-2c7f4a124842");
                EMG0Characteristic = peripheral.characteristic("d5060105-a904-deb9-4748-2c7f4a124842");
                EMG1Characteristic = peripheral.characteristic("d5060205-a904-deb9-4748-2c7f4a124842");
                EMG2Characteristic = peripheral.characteristic("d5060305-a904-deb9-4748-2c7f4a124842");
                EMG3Characteristic = peripheral.characteristic("d5060405-a904-deb9-4748-2c7f4a124842");

            } else {
                //Disconnect
                peripheral.disconnect();
                return;
            }
            
            //Send Command to Activate EMG Streaming 
            if(CommandCharacteristic){
                myohw_command_set_mode_t cmd;
                //String cmdint;
                cmd.header.command = 0x01;
                cmd.header.command = 0x01;
                cmd.header.payload_size = 0x03;
                cmd.emg_mode = 0x02;
                cmd.imu_mode = 0x00;
                cmd.classifier_mode = 0x00;
                //cmdint = *cmd;
                CommandCharacteristic.writeValue((uint8_t *)&cmd, sizeof(cmd)); 
            }

           //Subscribe to all EMG channels
           if (!EMG0Characteristic.subscribe() || !EMG1Characteristic.subscribe() || !EMG2Characteristic.subscribe() || !EMG3Characteristic.subscribe())
                return;
        }
    } else {
        //Read Updated EMG Values
        myohw_emg_data_t EMG;
        int Current_EMG[10] = {0,0,0,0,0,0,0,0,0,0};
        int window[3] = {1,1,1};
        
        if (EMG0Characteristic.valueUpdated() && EMG1Characteristic.valueUpdated() && EMG2Characteristic.valueUpdated() && EMG3Characteristic.valueUpdated()) {
        
            int j = 1;
            EMG0Characteristic.readValue((uint8_t *)&EMG, sizeof(EMG));
            for(int i=0;i < 8;i++){
                Current_EMG[j] += abs(EMG.sample1[i]);
                Current_EMG[j+1] += abs(EMG.sample2[i]);           
            }
            j = 3;
            EMG1Characteristic.readValue((uint8_t *)&EMG, sizeof(EMG));
            for(int i=0;i < 8;i++){
                Current_EMG[j] += abs(EMG.sample1[i]);
                Current_EMG[j+1] += abs(EMG.sample2[i]);             
           }
           j = 5;
           EMG1Characteristic.readValue((uint8_t *)&EMG, sizeof(EMG));
           for(int i=0;i < 8;i++){
                Current_EMG[j] += abs(EMG.sample1[i]);
                Current_EMG[j+1] += abs(EMG.sample2[i]);             
           }
           j = 7;
           EMG1Characteristic.readValue((uint8_t *)&EMG, sizeof(EMG));
           for(int i=0;i < 8;i++){
                Current_EMG[j] += abs(EMG.sample1[i]);
                Current_EMG[j+1] += abs(EMG.sample2[i]);             
           }
           int *Enveloped = convole(Current_EMG,window,10,3);
           
           int Max = Enveloped[0];
           for(int i=1;i < 8;i++){
                if (Enveloped[i] > Max)
                    Max = Enveloped[i];           
           }
           if(Max > THRESH){
              String command = ""; 
              emsSystem.doCommand(&command);
           }
        }
    }
}

int* convole(int h[], int x[], int lenH, int lenX, int* lenY) {
  int nconv = lenH+lenX-1;
  (*lenY) = nconv;
  int i,j,h_start,x_start,x_end;
  int *y = (int*) calloc(nconv, sizeof(int));
  for (i=0; i<nconv; i++)
  {
    x_start = MAX(0,i-lenH+1);
    x_end   = MIN(i+1,lenX);
    h_start = MIN(i,lenH-1);
    for(j=x_start; j<x_end; j++)
    {
      y[i] += h[h_start--]*x[j];
    }
  }
  return y;
}



//Convert-functions for HEX-Strings "4D"->"M"
int convertTwoHexCharsToOneByte(String &s, uint8_t index) {
	int byteOne = convertHexCharToByte(s.charAt(index));
	int byteTwo = convertHexCharToByte(s.charAt(index + 1));
	if (byteOne != -1 && byteTwo != -1)
		return (byteOne << 4) + byteTwo;
	else {
		return -1;
	}
}

int convertHexCharToByte(char hexChar) {
	if (hexChar >= 'A' && hexChar <= 'F') {
		return hexChar - 'A' + 10;
	} else if (hexChar >= '0' && hexChar <= '9') {
		return hexChar - '0';
	} else {
		return -1;
	}
}

//For testing
void debug_Toolkit(char c) {
	if (c == '1') {
		if (emsChannel1.isActivated()) {
			emsChannel1.deactivate();
			debug_println(F("\tEMS: Channel 1 inactive"));
		} else {
			emsChannel1.activate();
			debug_println(F("\tEMS: Channel 1 active"));
		}
	} else if (c == '2') {
		if (emsChannel2.isActivated()) {
			emsChannel2.deactivate();
			debug_println(F("\tEMS: Channel 2 inactive"));
		} else {
			emsChannel2.activate();
			debug_println(F("\tEMS: Channel 2 active"));
		}
	} else if (c == 'q') {
		digitalPot.setPosition(1, digitalPot.getPosition(1) + 1);
		debug_println(
				String(F("\tEMS: Intensity Channel 1: "))
						+ String(digitalPot.getPosition(1)));
	} else if (c == 'w') {
		digitalPot.setPosition(1, digitalPot.getPosition(1) - 1);
		debug_println(
				String(F("\tEMS: Intensity Channel 1: "))
						+ String(digitalPot.getPosition(1)));
	} else if (c == 'e') {
		//Note that this is channel 3 on Digipot but EMS channel 2
		digitalPot.setPosition(3, digitalPot.getPosition(3) + 1);
		debug_println(
				String(F("\tEMS: Intensity Channel 2: "))
						+ String(digitalPot.getPosition(3)));
	} else if (c == 'r') {
		//Note that this is channel 3 on Digipot but EMS channel 2
		digitalPot.setPosition(3, digitalPot.getPosition(3) - 1);
		debug_println(
				String(F("\tEMS: Intensity Channel 2: "))
						+ String(digitalPot.getPosition(3)));
	}
}
