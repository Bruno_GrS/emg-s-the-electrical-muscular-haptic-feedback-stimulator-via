#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Machine Learning Controlers - Hyperdimensional Computing Gesture Controler
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
#

import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import math

class HD_MODEL():
    def __init__(self,Dimension,Num_channels,Amp_channels,N_labels,Window_size,EMG_Train,Labels_Train,Min_distance):
        # Standart Hyperdimension Definitons
        self.D = Dimension                                              # Dimension of the Hypervectors
        self.Num_channels = Num_channels                                # Number of EMG Channels
        self.Amp_channels = Amp_channels                                # Maximum Quatized amplitude of the Channels 
        self.Window_size = Window_size                                  # Size of the time enconding window
        self.N_labels = N_labels                                        # Number of Gestures encoded
        # Trainging Definitons
        self.EMG = EMG_Train                                            # EMG data (columns for channels, axis for timestamp)
        self.Labels = Labels_Train.astype(int)                          # Gesture labels for each timestamp in EMG(needs the same len as EMG data )
        self.Min_distance = Min_distance                                # Minumum distance(cosine distance) for to HV to be considered equals (from 0 to 1)        
        # Memories Definitions 
        self.Item_memory = np.zeros((self.D,self.Num_channels))
        self.Cont_Item_memory = np.zeros((self.D,self.Amp_channels))
        self.Associative_memory = np.zeros((self.D,self.N_labels)) # Just this creates the associative memory.    

    def Gen_HV_vector(self):
        HV = np.random.randint(2, size=self.D)
        HV[HV == 0] = -1
        return HV

    def Create_Item_Memory(self):
        for i in range(self.Num_channels):
            self.Item_memory[:,i] = self.Gen_HV_vector()

    def Create_Cont_Item_Memory(self):
        step = math.floor(self.D  /2 /self.Amp_channels)
        Hv = self.Gen_HV_vector()
        permutation = np.random.permutation(self.D)
        Sindx = 0
        Eindx = step
        for i in range(self.Amp_channels):
            self.Cont_Item_memory[:,i] = Hv
            Sindx += step
            Eindx += step 
            Hv[permutation[Sindx:Eindx]] = Hv[permutation[Sindx:Eindx]] * (-1)
            
    def Train_Associative_Memory(self):
        self.Stored_Patterns = np.zeros(self.N_labels + 1) # Stores how much patterns are stored for each label and total patters learned on
        for x in range(self.EMG.shape[0]):
            ngram = self.Compute_Ngram(self.EMG[x,:,:])
            distance = cosine_similarity(np.expand_dims(ngram, axis=0), np.expand_dims(Normalize(np.copy(self.Associative_memory[:,self.Labels[x]])), axis=0))
            print(distance)
            if distance < self.Min_distance or math.isnan(distance):
                self.Associative_memory[:,self.Labels[x]] = self.Associative_memory[:,self.Labels[x]] + ngram
                self.Stored_Patterns[self.Labels[x]] += 1
            self.Stored_Patterns[-1] += 1
        for x in range(self.Associative_memory.shape[1]):
            Normalize(self.Associative_memory[:,x])
            
    def Compute_Ngram(self,data): # O input de data é composto de N valores temporais
        ngram = np.zeros(self.D)
        temp = np.zeros(self.D)
        for i in range(self.Window_size):
            temp = np.multiply(self.Cont_Item_memory[:,(data[i,0]-1)],self.Item_memory[:,0])
            for j in range(1,self.Num_channels):
                temp = temp + np.multiply(self.Cont_Item_memory[:,(data[i,j]-1)],self.Item_memory[:,j])
            if i == 0:
                ngram = temp
            else:
                ngram = np.multiply(ngram,np.roll(temp,i))
            temp = np.zeros(self.D)
        Normalize(ngram)
        return ngram

    def Predict_Single_contraction(self,Test,Label): # The input needs lenght equal to N
        distances = list()        
        TestHV = self.Compute_Ngram(Test)
        for i in range(self.N_labels):
            distance = cosine_similarity(np.expand_dims(self.Associative_memory[:,i], axis=0), np.expand_dims(TestHV, axis=0))
            distances.append(distance)
        distances = np.nan_to_num(distances) # Fail safe if a data set did not contain one of the labels
        prediction = list(distances).index(np.amax(distances))
        return prediction

    def Predict_Data(self,Test_EMG,Labels):
        Y_pred = list()
        for x in range(Test_EMG.shape[0]):
            Y_pred.append(self.Predict_Single_contraction(Test_EMG[x,:,:],Labels[x]))   
               
        # Confusion_matrix = confusion_matrix(Labels, Y_pred)
        # pprint.pprint(Confusion_matrix)
        # sup = Confusion_matrix.astype('float') / Confusion_matrix.sum(axis=1)[:, np.newaxis]
        # ACC_perclass = sup.diagonal()
        # ACC_full = Confusion_matrix.diagonal().sum() / Confusion_matrix.sum()
        # print("Accuracy: ")
        # print(ACC_perclass,"-->",ACC_full)
        # print("Stored Patterns:",self.Stored_Patterns[0:self.Stored_Patterns.shape[0]-1])
        # print("Tested: ",self.Stored_Patterns[-1])

#Support Functions
def Normalize(HV):
    HV[HV > 0] = 1
    HV[HV < 0] = -1
    zind = np.where(HV == 0)[0]
    HV[zind] = np.array([np.random.choice([1, -1]) for _ in zind])
    return HV

def strided_app(EMG,labels, L, S ):
    nrowsl = ((labels.shape[0]-L)//S)+1
    nrowse = ((EMG.shape[0]-L)//S)+1
    ln = labels.strides
    En = EMG.strides
    E = np.lib.stride_tricks.as_strided(EMG, shape=(nrowse,L,EMG.shape[1]),strides=(S*En[0],En[0],En[1]))
    lan = np.lib.stride_tricks.as_strided(labels, shape=(nrowsl,L), strides=(S*ln[0],ln[0]))
    equals = np.equal.reduce(lan, axis=1)
    B = np.where(equals==True)[0]
    return E[B],lan[B,0]
    
def Normalizator_Spliter(Training,Testing,Len,Stride,Amp_channels):  
    #Separa as features das classes
    X_train = Training[:,1:Training.shape[1]] 
    Y_train = Training[:,0]
    X_test = Testing[:,1:Testing.shape[1]]  
    Y_test = Testing[:,0]
    #Envelop
    X_train = Envelop(X_train)
    X_test = Envelop(X_test)
    #Binarizer
    bina = KBinsDiscretizer(n_bins=Amp_channels, encode='ordinal', strategy='uniform')
    X_train = bina.fit_transform(X_train)
    X_test = bina.fit_transform(X_test)
    #quebra os dados em janelas com as strides certas
    X_train,Y_train = strided_app(X_train,Y_train,Len,Stride)
    X_test,Y_test = strided_app(X_test,Y_test,Len,Stride)   
    #Mistura os sets para os futuros bachs nao terem só algumas pessoas/contraçoes
    ind =  np.random.permutation(X_train.shape[0])
    np.take(X_train,ind,axis=0,out=X_train)
    np.take(Y_train,ind,axis=0,out=Y_train)
    ind =  np.random.permutation(X_test.shape[0])
    np.take(X_test,ind,axis=0,out=X_test)
    np.take(Y_test,ind,axis=0,out=Y_test)   
    return X_train.astype(int),Y_train.astype(int),X_test.astype(int),Y_test.astype(int)