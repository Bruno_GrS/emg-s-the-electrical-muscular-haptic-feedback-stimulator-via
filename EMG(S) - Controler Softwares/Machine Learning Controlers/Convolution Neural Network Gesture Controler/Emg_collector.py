#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Machine Learning Controlers - Convolution Neural Network Gesture Controler
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

import myo #Imports the Myo-Python a CFFI wrapper for the Thalmic Myo SDK
from collections import deque #Imports double ended queue structure
from threading import Lock, Thread # Imports Thread and lock to work on multiple Threads
import numpy as np

class EmgCollector(myo.DeviceListener): #Basic Class from Myo-Phyton to listen to the Armband on a separated Thread
    """
    Collects EMG data in queue and waits for call to send it either in RAW or Enveloped format
    W - Size of the queue
    """

    def __init__(self,W):
        self.lock = Lock()
        self.connected = False
        self.streaming = False
        self.emg_data_queue = deque(maxlen=W)   

    def Clear_queue(self):
        with self.lock:
            self.emg_data_queue.clear()     

    def Send_data(self,N):
        """
        Sends data and free`s the queue. 
        N - the ammount of data to send. Should be smaler than 1000
        If there is not enought data returns None
        """
        if self.connected == False:
            return None
        if self.streaming == False:
            self.Toggle_streaming()
        with self.lock:
            data = list()
            if len(self.emg_data_queue) < N:
                return None
            # data = np.array(self.emg_data_queue)
            [data.append(self.emg_data_queue.popleft()) for _ in range(N)]
            return np.array(data).T
    
    def Toggle_streaming(self):
        """
        Subscribe/Unsubscribe to EMG data.
        """
        if self.connected == True and self.streaming == False:
            self.streaming = True
            self.event.device.stream_emg(True)
        else:
            self.streaming = False
            self.event.device.stream_emg(False)

    def on_connected(self, event): #Caled when object is created
        self.connected = True
        self.event = event
    
    def on_unpaired(self, event):
        self.streaming = False
        with self.lock:
            self.Clear_queue()
    
    def on_disconnected(self):
        self.connected = False
        self.streaming = False
        with self.lock:
            self.Clear_queue()

    def on_emg(self, event): # Called everytime the Armand sends some EMG data
        with self.lock:
            data = event.emg
            self.emg_data_queue.append(data)