# On-Off Controler via Threshold

## Description

This program is shows an example of one of the simplest way a sEMG Signal can be used to generate a control signal.
A Threshold Controler or On-Off Controler, the second name comes from the fact this method allows a 2 states such like and On or Off switch.
The program simply calculates the envelop of the average signal, betwen all sensors, and for every point checks wheter it is higher os lower than a numerical threshoold
In case the value is greater it activates the estimulation ultil a lower value is detected.

## Dependencies

* kivy
* kivy_garden
* pandas
* numpy
* pyserial
* myo-python
