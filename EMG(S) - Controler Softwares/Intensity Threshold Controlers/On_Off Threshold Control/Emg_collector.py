#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Intensity Controlers - On Off Threshold Controler
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

import myo #Imports the Myo-Python a CFFI wrapper for the Thalmic Myo SDK
from collections import deque #Imports double ended queue structure
from threading import Lock, Thread # Imports Thread and lock to work on multiple Threads
import numpy as np

class EmgCollector(myo.DeviceListener): #Basic Class from Myo-Phyton to listen to the Armband on a separated Thread
    """
    Collects EMG data in queue and waits for call to send it either in RAW or Enveloped format
    W - Size of the enveloping window
    """

    def __init__(self,W):
        self.lock = Lock()
        self.connected = False
        self.streaming = False
        self.emg_data_queue = deque(maxlen=1000)
        self.W = W 
        self.box = np.ones((self.W,))/self.W          

    def Send_data_Enveloped(self):
        """
        Envelops all data in queue and sends ONLY THE AVERAGE The queue is cleared.
        """
        with self.lock:
            if len(self.emg_data_queue) < self.W:
                return None
            data = np.array(self.emg_data_queue)
            [self.emg_data_queue.popleft() for _ in range(10)]
            conv = np.array(np.convolve(np.absolute(data[:,-1]), self.box, mode='valid')).T
            return conv[-10:-1]
    
    def Toggle_streaming(self):
        """
        Subscribe/Unsubscribe to EMG data.
        """
        if not self.connected:
            return False
        if self.connected == True and self.streaming == False:
            self.streaming = True
            self.event.device.stream_emg(True)
        else:
            self.streaming = False
            self.event.device.stream_emg(False)

    def on_connected(self, event): #Caled when object is created
        self.connected = True
        self.event = event

    def on_emg(self, event): # Called everytime the Armand sends some EMG data
        with self.lock:
            data = event.emg
            data.append(sum(event.emg))
            self.emg_data_queue.append(data)