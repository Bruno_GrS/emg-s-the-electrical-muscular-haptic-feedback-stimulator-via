#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Dataset Preparation Script - PINCH
# Made for Phyton 3.8
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

### Tranforms the Donati,Elisa - PINCH Dataset in the right format .csv file used by the other programs on this repository.
# To use this code follow the steps bellow:
# 1) Donwload the dataset at: https://zenodo.org/record/3194792 as a .zip file
# 2) Unpack the Pinch folder on a temporary location 
# 3) Chose any number of subjects and sessions to be included and put the .npy files on the same folder as this script (main.py). 
# Be aware that you have to chose both the ***_emg.npy and the ***_ann.npy for every subject/session you select. 
# THE CODE MIGHT STILL RUN BUT THE RESULTS WILL NOT BE CORRECT
# 4) Run this script
# 5) A file called EMG_Test_Data.csv should appear on the same folder. This is the file to be used on the other programs of this repository.


import numpy as np
import os
import pandas as pd

if __name__ == '__main__':
    path = '.'
    directory_contents = os.listdir(path) #Lists all content of the directory
    EMG_files = list()
    Classes_files = list()
    last_subject = 0

    # Finds all subjects and sessions in the folder
    for x in directory_contents:
        if x[-7:] == "emg.npy":
            EMG_files.append(x)
        if x[-7:] == "ann.npy":
            Classes_files.append(x)

    # Creates the necessary dataframes
    col = ["emg0","emg1","emg2","emg3","emg4","emg5","emg6","emg7","Class","Subject"]
    df = pd.DataFrame(columns=col)
    temp = pd.DataFrame(columns=col)

    # Runs the files present, loads, and ajust the formats to the final dataframe
    for x,y in zip(EMG_files,Classes_files):
        EMG = np.load(x)
        Class = np.load(y,allow_pickle=True).astype(str)
        for i,j in enumerate(col[0:8]):
            temp[j] = EMG[:,i].astype(float)
        temp["Class"] = Class
        temp["Subject"] = np.full(Class.shape[0],int(x[(x.find("_")-1)]),dtype=int)
        df = df.append(temp,ignore_index=True)
        temp.drop(temp.index, inplace=True)

    # Convert string classes to numeric
    df.loc[df["Class"] == "none","Class"] = 0
    df.loc[df["Class"] == "Pinch1","Class"] = 1
    df.loc[df["Class"] == "Pinch2","Class"] = 2
    df.loc[df["Class"] == "Pinch3","Class"] = 3
    df.loc[df["Class"] == "Pinch4","Class"] = 4
    df = df.astype({'Class': 'int','Subject': 'int'})

    # Saves final dataframe
    df.to_csv("EMG_Test_Data.csv")

