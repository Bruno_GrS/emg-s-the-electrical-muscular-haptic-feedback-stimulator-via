/**
* EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
* Arduino Standalone Controler
* Made for Ardunio BLE 33
* Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
* <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
* Licensed under the MIT License. See License.txt in root.
*
*/

#if !defined (BLEMODULE)
#define BLEMODULE

static String Desired_UUID = "d5060001-a904-deb9-4748-2c7f4a124842";

typedef struct  __attribute__ ((__packed__)) myohw_command_header {
    uint8_t command;        ///< Command to send. See myohw_command_t.
    uint8_t payload_size;   ///< Number of bytes in payload.
} myohw_command_header_t;

typedef struct __attribute__ ((__packed__)) myohw_command_set_mode {
    myohw_command_header_t header; ///< command == myohw_command_set_mode. payload_size = 3.
    uint8_t emg_mode;              ///< EMG sensor mode. See myohw_emg_mode_t.
    uint8_t imu_mode;              ///< IMU mode. See myohw_imu_mode_t.
    uint8_t classifier_mode;       ///< Classifier mode. See myohw_classifier_mode_t.
} myohw_command_set_mode_t;

typedef struct __attribute__ ((__packed__)) myohw_emg_data {
    int8_t sample1[8];       ///< 1st sample of EMG data.
    int8_t sample2[8];       ///< 2nd sample of EMG data.
} myohw_emg_data_t;

#endif
