#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Machine Learning Controlers - Convolution Neural Network Gesture Controler
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

#Kivy imports
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.stacklayout import StackLayout
from kivy_garden.graph import Graph, MeshLinePlot, LinePlot, ScatterPlot
from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label    
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from kivy.uix.image import Image
from kivy.clock import Clock, mainthread
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
import kivy.properties as kyprops
from kivy.graphics import Line, InstructionGroup
import serial
# import cv2

#Imports Gerais 
import os
import random
import pandas as pd
import numpy as np
from collections import deque
from functools import partial
import threading
import ctypes

#machine Learning Imports

from Emg_collector import EmgCollector
from DataHandler import DataHandler
import myo
   
class MenuScreen(Screen):
    loadfile = kyprops.ObjectProperty(None)
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()
    task = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(MenuScreen,self).__init__(**kwargs)
        self.DataH = DataHandler()
        self.EMS = serial.Serial()
        self.EMS.baudrate = 115200
        self.EMS.port = 'COM3'
        self.Class_boxes, self.Prepros_boxes, self.Default, self.Traing_params = list(), list(), list(), list()
        self.ig = InstructionGroup()
        x1,x2,x3,x4 = 25,410,620,1010 
        # self.lines = [[x1, 595, x2, 595],[x3, 595, x4, 595],[x1, 519, x2, 519],[x3, 519, x4, 519],[x1, 412, x2, 412],[x3, 412, x4, 412],[x1, 234, x2, 234],[x3, 234, x4, 234]]
        self.lines = [[x1, 598, x2, 598],[x3, 598, x4, 598],[x1, 453, x2, 453],[x3, 453, x4, 453],[x1, 310, x2, 310],[x3, 310, x4, 310],[x1, 200, x4, 200]]
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component\n"
        self.helpt = ("[size=20][b]Feature Selection and Extraction[/b][/size]\n\n")
        Window.bind(mouse_pos=self.Check_Mouse)

    def on_enter(self, *args):
        Clock.schedule_once(lambda dt: self.Create_layout())

    def Create_layout(self):
        [self.ig.add(Line(points=c)) for c in self.lines]
        self.canvas.add(self.ig)
        self.Default.append(Label(text = "Classes: ...... "))
        self.ids.B_classes.add_widget(self.Default[0])
        for x,f in enumerate(self.DataH.prepross_all):
            self.ids.B_prepros.add_widget(Label(text = f))
            self.Prepros_boxes.append(CheckBox())
            self.ids.B_prepros.add_widget(self.Prepros_boxes[x])
        self.Test_MyoConection()
        self.Test_EMSConection()   

    def Test_MyoConection(self):    
        try:
            Listener.Toggle_streaming()
            self.ids.B_Myo.text = "[color=#52ef00]MyoConnect Running, Armband Connected[/color]"
            self.ids.B_MyoC.active = True
        except:
            Clock.schedule_once(lambda dt: self.Test_MyoConection(),5)

    def Test_EMSConection(self):
        try:
            self.EMS.open()
            self.ids.B_EMS.text = "[color=#52ef00]EMS via Serial Connected and Listening[/color]"
            self.ids.B_EMSC.active = True
        except:
            Clock.schedule_once(lambda dt: self.Test_EMSConection(),5)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component\n"
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in\n"
    
    def dismiss_popup(self):
        self._popup.dismiss()

    def show_load(self):
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup,start_path=os.path.dirname(os.path.abspath(__file__)))
        self._popup = Popup(title="Load file", content=content, size_hint=(0.9, 0.9),auto_dismiss=False)
        self._popup.open()

    def load(self, path, filename):
        self.dismiss_popup()
        if self.DataH.Load_dataset(filename[0]):
            content = Error(close=self.dismiss_popup,content = "Not Able to Load Data")
            self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
            self._popup.open()
        else:
            self.ids.Lpath.text = "...\\"+"\\".join(filename[0].split("\\")[-2:-1])+"\\"+filename[0].split("\\")[-1]
            self.Update_Page()
    
    def Update_Page(self):
        self.ids.B_classes.remove_widget(self.Default[0])
        for x,f in enumerate(np.unique(self.DataH.Raw_Data["Class"])):
            self.ids.B_classes.add_widget(Label(text = "Class: "+str(int(f))))
            self.Class_boxes.append(CheckBox())
            self.ids.B_classes.add_widget(self.Class_boxes[x])

    def Confirm_selections(self):
        if self.DataH.Dataset_Loaded:
            classes_selected = list()
            for x in np.unique(self.DataH.Raw_Data["Class"]):
                classes_selected.append(self.Class_boxes[int(x)].active)
            if sum(classes_selected) < 2:
                content = Error(close=self.dismiss_popup,content = "Select At Least 2 Classes")
                self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
                self._popup.open()
                return
            # if  not self.ids.B_MyoC.active:
            #     content = Error(close=self.dismiss_popup,content = "MyoArmband not Connected")
            #     self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
            #     self._popup.open()
            #     return 
            # if  not self.ids.B_EMSC.active:
            #     content = Error(close=self.dismiss_popup,content = "EMS not Connected")
            #     self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
            #     self._popup.open()
            #     return
            #Start calculating/Training
            self.thread = threading.Thread(target=self.Calculate_all,args=(classes_selected,))
            self.thread.start()
            self.task = "PreProssesing \n Should Not Take Long"
            self.pop = Popup(title="Preparing Classifier", content=Label(text=self.task),size_hint=(None, None), size=(400, 400),auto_dismiss=False)
            self.pop.open()
            self.clock = Clock.schedule_interval(lambda dt: self.Update_popup(),1)
        else:
            content = Error(close=self.dismiss_popup,content = "Load a Valid Dataset")
            self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
            self._popup.open()
            return  

    def Calculate_all(self,classes_selected):
        self.DataH.Update_All_Selections((self.Prepros_boxes[0].active,self.Prepros_boxes[1].active))
        self.task = "Epoching Data \n Should Not Take Long"
        self.DataH.Epoch_Data()
        self.task = "Selecting Classes \n Should Not Take Long"
        self.DataH.Select_Classes(classes_selected)
        self.task = "PreProssesing \n Should Not Take Long"
        self.DataH.Preprosses_Data()
        self.DataH.Window_Scale()
        self.task = "Training Neural Network \n May Take a While"
        self.DataH.Train_Classifier(8,10) #------------------------------------------------------------------------------


    def Update_popup(self):
        if not self.thread.is_alive():
            self.pop.dismiss()
            self.clock.cancel()
            self.manager.current = "Classification"
    

class ClassificationScreen(Screen):
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(ClassificationScreen,self).__init__(**kwargs)
        self.plotC = [[0, 0.4470, 0.7410],[0.8500, 0.3250, 0.0980],[0.9290, 0.6940, 0.1250],[0.4940, 0.1840, 0.5560],[0.4660, 0.6740, 0.1880],[0.3010, 0.7450, 0.9330],[0.6350, 0.0780, 0.1840],[0.25, 0.25, 0.25]]
        self.Base_plot = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.lenPlot = 1000
        self.lenWindow = 420
        self.EMG = tuple(deque(maxlen=self.lenPlot) for _ in range(8))
        self.Class_window = tuple(deque(maxlen=self.lenWindow) for _ in range(8))
        [self.EMG[x].append(j) for i in range(self.lenPlot) for x,j in enumerate(np.zeros((8,1000))[:,i])]
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component\n"
        self.helpt = ("[size=20][b]Feature Selection and Extraction[/b][/size]\n\n")
        self.thread = threading.Thread(target=self.Online_Classification)
        Window.bind(mouse_pos=self.Check_Mouse)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component\n"
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in\n"

    def on_pre_enter(self, *args):
        self.DataH = self.manager.get_screen("Menu").DataH
        self.EMS = self.manager.get_screen("Menu").EMS
        Listener.Clear_queue()
        [self.ids.graph.add_plot(self.Base_plot[x]) for x,_ in enumerate(self.Base_plot)]
        self.ValueClock = Clock.schedule_interval(lambda dt: self.Get_value(), 0.1)

    def on_enter(self, *args):
        Clock.schedule_once(lambda dt: self.Create_layout())
        self.Class_imgs = list()

    def Create_layout(self):
        for x,f in enumerate(np.unique(self.DataH.Raw_Data["Class"])):
            name = str(int(f+1))+".png"
            self.Class_imgs.append(Image(source=name,size=(100,100)))
            self.ids.B_IMG.add_widget(self.Class_imgs[x])
            text = "Class: " + str(int(f))
            self.ids.B_IMG.add_widget(Label(text=text))

    def Get_value(self):
        data = Listener.Send_data(25)
        if data is not None:
            [self.Class_window[x].append(j) for i in range(data.shape[1]) for x,j in enumerate(data[:,i])]
            [self.EMG[x].append(j) for i in range(data.shape[1]) for x,j in enumerate(data[:,i])]
            for x in range(data.shape[0]):
                self.Base_plot[x].points = [(i, j) for i, j in enumerate(self.EMG[x])]
            if not self.thread.is_alive():
                self.thread = threading.Thread(target=self.Online_Classification)
                self.thread.start()

    def Online_Classification(self):
        if len(self.Class_window[0]) > (self.lenWindow-1):
            Data = np.zeros((8,self.lenWindow),dtype='uint8')
            for x in range(Data.shape[0]):
                Data[x,:] = np.array(list(self.Class_window[x]))
            Data = self.DataH.Preprosses_Test(Data)
            Data = self.DataH.Window_Train(Data)
            prediction =  self.DataH.Test_Classifier(Data)
            Clock.schedule_once(partial(self.update_image,prediction))

    def update_image(self,prediction, *largs):
        for imgs in self.Class_imgs:
            imgs.color = [1, 1, 1, 1]
        self.Class_imgs[int(prediction)].color = [0, 1, 0, 1]


#Suport Functions

class Error(FloatLayout):
    close = kyprops.ObjectProperty(None)
    content = kyprops.StringProperty(None)

class LoadDialog(FloatLayout):
    load = kyprops.ObjectProperty(None)
    cancel = kyprops.ObjectProperty(None)
    start_path = kyprops.StringProperty(None)

class Manager(ScreenManager):
    pass

# kv = Builder.load_file("MyoEletricControler.kv")
class MyoEletricControlerApp(App):
    def build(self):
        Window.size = (1280,720)
        m = Manager(transition=NoTransition())
        return m


if __name__ == '__main__':
    myo.init()
    try:
        hub = myo.Hub()
    except :
        ctypes.windll.user32.MessageBoxW(0, "Is Myo Connect running?\nOpen MyoConnect before running", "Unable to connect to Myo Connect", 0)
        quit()
    Listener = EmgCollector(100) #Creates Object that listens to the Armband
    with hub.run_in_background(Listener.on_event): # The whole window is run inside the Listerner Context
        MyoEletricControlerApp().run()