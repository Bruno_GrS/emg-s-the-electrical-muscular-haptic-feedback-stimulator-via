# Datasets Preparation

This folder holds three small scripts that can help you read and modify the format of three online public datasets so they can be used on the other programs on this dataset. It also cobtains a more complex program, with a visual interface, that guides its user on aquiring a personal dataset of its own gestures so they can be used for classification purposes.

## Datasets

All datasets cited bellow were publicly found on the internet. We make no garateas about their fit to any use case.
To download each follow the links presented. We do not carry any of them in this reposotory. YOU NEED TO DOWNLOAD THEM FOR YOURSELF
To transform they so they can be used follow the instructions inside each python scripts.

### Dataset - Donati,Elisa - PINCH

[Download](https://zenodo.org/record/3194792)

This Dataset contains recordings of 22 subjects performing 4 PINCH movements (4 pinches between thumb and index/middle/ring/pinky finger) plus rest. Each subject performed 3 sessions, where each hand gesture was recorded 5 times, each lasting for 2s. Between the gestures a relaxing phase of 1s is present where the muscles could go to the rest position, removing any residual muscular activation. Mesurements were made using the Myo Armband.

If used for publication cite the original source:
Donati, Elisa. (2019). EMG from forearm datasets for hand gestures recognition [Data set]. Zenodo. http://doi.org/10.5281/zenodo.3194792

### Dataset - Donati,Elisa - ROSHAMBO

[Download](https://zenodo.org/record/3194792)

This Dataset contains recordings of 10 subjects performing 3 movements (based of the classic game: rock, paper, scissors) plus rest. Each subject performed 3 sessions, where each hand gesture was recorded 5 times, each lasting for 2s. Between the gestures a relaxing phase of 1s is present where the muscles could go to the rest position, removing any residual muscular activation. Mesurements were made using the Myo Armband.

Full details can be found in:

Donati, Elisa, et al. "Processing EMG signals using reservoir computing on an event-based neuromorphic system." 2018 IEEE Biomedical Circuits and Systems Conference (BioCAS). IEEE, 2018

If used for publication cite the original source:
Donati, Elisa. (2019). EMG from forearm datasets for hand gestures recognition [Data set]. Zenodo. http://doi.org/10.5281/zenodo.3194792

### Dataset - UCI Machine Learning Repository

[Download](https://archive.ics.uci.edu/ml/datasets/EMG+data+for+gestures#)

This Dataset contains recordings of 36 subjects performing 6 valid movements (list bellow) plus rest and a general unvalid movement (data not categorized).The subjects performed two series. Each gesture was performed for 3 seconds with a pause of 3 seconds between gestures.

0 - unmarked data,
1 - hand at rest,
2 - hand clenched in a fist,
3 - wrist flexion,
4 - wrist extension,
5 - radial deviations,
6 - ulnar deviations,
7 - extended palm (the gesture was not performed by all subjects).

Full details in:
Lobov S., Krilova N., Kastalskiy I., Kazantsev V., Makarov V.A. Latent Factors Limiting the Performance of sEMG-Interfaces. Sensors. 2018;18(4):1122. doi: 10.3390/s18041122

If used for publication cite the original source:
Supported by the Ministry of Education and Science of the Russian Federation in the framework of megagrant allocation in accordance with the decree of the government of the Russian Federation â„–220, project â„– 14.Y26.31.0022

## Personal Dataset

This small GUI helps the user in collecting a personal dataset of 2-9 gestures of 1-50 tries for each. It them saves the gestures as a .csv file that can be used on the other programs in this
repository. This personal dataset can be used as a standalone training for various of the Machine learning algoritms but it could also be merged in other datasets to help the algoritms better learn about one specific user, thus improving their performance. We use some stock images of numbers as the templates for each gesture, we encorage the user to change them for more meaninfull images to make the recording easier. To do so just replace the images in the folder *labels_img*. The new images should be named, in order, from 1 to 9 (1.PNG, 2.PNG, etc).
