#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Intensity Controlers - Multiple Thresholds Controler
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

#Kivy imports
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.button import Button    
from kivy.uix.popup import Popup
from kivy.uix.boxlayout import BoxLayout
from kivy_garden.graph import MeshLinePlot
from kivy.clock import Clock
from kivy.core.window import Window
import kivy.properties as kyprops
import serial

#Imports Gerais 
import numpy as np
from collections import deque
import ctypes

from Emg_collector import EmgCollector # type: ignore
import myo


class Logic(BoxLayout):

    #StringProperty definitons
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        self.Selected_Emgs = [0,4,7]
        self.Sensors = [f'Sensor {f}' for f in list(set([0,1,2,3,4,5,6,7]) - set(self.Selected_Emgs))]
        super(Logic, self).__init__(**kwargs)
        self.Threshold = [None,None,None]
        self.Intensity = [100,50,25]
        self.Allow_cmds = False
        self.plotC = [[0.4940, 0.1840, 0.5560, 0.9],[0.8500, 0.3250, 0.0980, 0.9],[0.9290, 0.6940, 0.1250, 0.9]]
        self.thresC = [[0.4940, 0.1840, 0.5560, 1],[0.8500, 0.3250, 0.0980, 1],[0.9290, 0.6940, 0.1250, 1]]
        self.lenPlot = 1000
        self.Base_plot = tuple(MeshLinePlot(color=c) for _,c in enumerate(self.plotC))
        self.Threshold_plot = tuple(MeshLinePlot(color=c) for _,c in enumerate(self.thresC))
        self.ids.graph_1.add_plot(self.Base_plot[0])
        self.ids.graph_1.add_plot(self.Threshold_plot[0])
        self.ids.graph_2.add_plot(self.Base_plot[1])
        self.ids.graph_2.add_plot(self.Threshold_plot[1])
        self.ids.graph_3.add_plot(self.Base_plot[2])
        self.ids.graph_3.add_plot(self.Threshold_plot[2])
        self.EMG = tuple(deque(maxlen=self.lenPlot) for _ in range(9))
        self.time = 0
        self.EMS = serial.Serial()
        self.EMS.baudrate = 115200
        self.EMS.port = 'COM3'
        self.Connected = False
        self.filer = (" \n")
        self.text = "This is the Tips Box \nMove your mouse arround to see Tips about each interactible component " + self.filer
        self.helpt = ("[size=20][b]Multiple Treshold On Off Controler[/b][/size]\n\n"
        "[size=18][b]Introduction: [/b][/size]\n This program shows a diferent implementation of the On Off Threshold controler. "
        "This Code applies diferent thresholds to diferent signal channels in a multiple Threshold implementation. For each channel, "
        "two possible states exists a On state and a Off state and the systems decides betwen each state using the Threshold.\n"
        "This program shows three graphs where you can select three diferent sensors, one for each, and select a diferent threshold for each."
        "When the EMG value is superior than the Threshold a stimulation of 100%, 50% and 25% is generated from top to bottom.")
        Window.bind(mouse_pos=self.Check_Mouse)
 
    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about the program" + self.filer
        if self.ids.graph_1.collide_point(*y) or self.ids.graph_2.collide_point(*y) or self.ids.graph_3.collide_point(*y):
            self.text = "[size=18][b]These are the Channels Graphs[/b][/size] \n\nThe EMG signal from the Myo is ploted in these graphs" + self.filer
        if self.ids.B_S1.collide_point(*y) or self.ids.B_S2.collide_point(*y) or self.ids.B_S3.collide_point(*y):
            self.text = "[size=18][b]These are the Sensor selection Buttons[/b][/size] \n\nThese buttons are used to select the sensors from the Myo" + self.filer
        if self.ids.B_Thrup_1.collide_point(*y) or self.ids.B_Thrup_2.collide_point(*y) or self.ids.B_Thrup_3.collide_point(*y) or self.ids.B_Thrdw_1.collide_point(*y) or self.ids.B_Thrdw_2.collide_point(*y) or self.ids.B_Thrdw_3.collide_point(*y):
            self.text = "[size=18][b]These are the Threshold controler Buttons[/b][/size] \n\nUse these buttons to control the Threshold" + self.filer
        if self.ids.B_st.collide_point(*y):
            self.text = "[size=18][b]This is the Start/Stop Button[/b][/size] \n\nClicking this button starts/stops the EMG streaming and ploting on the main Graph" + self.filer
        if self.ids.B_cmds.collide_point(*y):
            self.text = "[size=18][b]This is the EMS conection Button[/b][/size] \n\nClicking this button connects the program to the EMS estimulator, and if the threshold its activated starts sending signals" + self.filer
        if self.ids.B_Thr.collide_point(*y):
            self.text = "[size=18][b]This is the Threshold Button[/b][/size] \n\nClicking this button activates a Threshold, if the sistems is connected to the EMS it will start sending estimulations" + self.filer

    def Start_serial(self):
        if not self.Connected:
            try:
                self.EMS.open()
            except:
                self.Show_Error("EMG(S) serial not Avaliable.\n\nMake sure the serial port is open and free", "EMG(S) not Connected")
            if self.EMS.is_open == True:
                self.ids.B_cmds.text = "[color=#52ef00]Connected[/color]"
                self.EMS.reset_input_buffer()
                self.Connected = True
        else:
            self.EMS.reset_input_buffer()
            self.EMS.close()
            self.ids.B_ems.text = "Connect to Estimulator"
            self.Connected = False
    
    def Toggle_Emg(self):
        if Listener.streaming == False:
            if not Listener.Toggle_streaming():
                self.Show_Error("Myo Armband Not Connected.\n\nMake sure the Myo is not asleep", "Myo not Connected")
                return
            Clock.schedule_interval(self.get_value, 0.1)
            self.ids.B_Thr.disabled = False
            self.ids.B_st.text = "[color=#52ef00]Start[/color]/Stop"  
        else:
            Listener.Toggle_streaming()
            Clock.unschedule(self.get_value)
            self.ids.B_Thr.disabled = True
            self.ids.B_st.text = "Start/[color=#e41e00]Stop[/color]"  
            [self.EMG[x].clear() for x in range(9)]
        
    def Show_Error(self,text1,text2):
        self.box_popup = BoxLayout(orientation = 'horizontal')
        self.box_popup.add_widget(Label(text = text1))
        self.box_popup.add_widget(Button(text = "Ok", on_press = lambda *args: self.popup_exit.dismiss(), size_hint = (0.215, 0.075)))
        self.popup_exit = Popup(title = text2, content = self.box_popup,size_hint=(None, None), size=(400, 400), auto_dismiss = True)  
        self.popup_exit.open()

    def Set_Threshold(self):
        for n in np.arange(3):
            if self.Threshold[n] == None:
                self.Threshold[n] = 50
                self.Threshold_plot[n].points = [(i, j) for i, j in enumerate(np.full((self.lenPlot),self.Threshold[n]))]
                self.ids.B_Thrup_1.disabled = self.ids.B_Thrdw_1.disabled = self.ids.B_Thrup_2.disabled = self.ids.B_Thrdw_2.disabled = self.ids.B_Thrup_3.disabled = self.ids.B_Thrdw_3.disabled = False   
            else:
                self.Threshold[n] = None
                self.Threshold_plot[n].points = [(i, j) for i, j in enumerate(np.full(1,0))]
                self.ids.B_Thrup_1.disabled = self.ids.B_Thrdw_1.disabled = self.ids.B_Thrup_2.disabled = self.ids.B_Thrdw_2.disabled = self.ids.B_Thrup_3.disabled = self.ids.B_Thrdw_3.disabled = True
                        
    def up_threshold(self,n):
        self.Threshold[n] = min([self.Threshold[n] + 10, 255])
        self.Threshold_plot[n].points = [(i, j) for i, j in enumerate(np.full((self.lenPlot),self.Threshold[n]))]
    def down_threshold(self,n):
        self.Threshold[n] = max([self.Threshold[n] - 10, 1])
        self.Threshold_plot[n].points = [(i, j) for i, j in enumerate(np.full((self.lenPlot),self.Threshold[n]))]

    def Update_Sensors(self,sensors,selected,n):
        self.Selected_Emgs[n] = sensors.index(selected)
        self.Sensors = [f'Sensor {f}' for f in list(set([0,1,2,3,4,5,6,7]) - set(self.Selected_Emgs))]
        self.ids.B_S1.values = self.ids.B_S2.values = self.ids.B_S3.values = self.Sensors
        [self.EMG[x].clear() for x in range(9)]

    def get_value(self, dt):
        data = Listener.Send_data_Enveloped()
        if data is not None:   
            [self.EMG[x].append(j) for i in range(len(data)) for x,j in enumerate(data[i])]
            for i,j in enumerate(self.Selected_Emgs):
                self.Base_plot[i].points = [(i, j) for i, j in enumerate(self.EMG[j])]
                if self.Threshold[i] is not None:
                    if self.Threshold[i] < self.EMG[i][-1]:
                        if self.EMS.is_open:
                            self.EMS.reset_input_buffer()
                            value = str(self.Intensity[i]) + "T010G\n"
                            comand = "C0I" + value
                            self.EMS.write(comand.encode())
                            comand = "C1I" + value
                            self.EMS.write(comand.encode())
                    else:
                        self.EMS.reset_output_buffer()


class MyoEletricControlerApp(App):
    def build(self):
        Window.size = (1280,720)
        return Logic() 

if __name__ == '__main__':
    myo.init()
    try:
        hub = myo.Hub()
    except :
        ctypes.windll.user32.MessageBoxW(0, "Is Myo Connect running?\nOpen MyoConnect before running", "Unable to connect to Myo Connect", 0)
        quit()
    Listener = EmgCollector(100) #Creates Object that listens to the Armband
    with hub.run_in_background(Listener.on_event): # The whole window is run inside the Listerner Context
        MyoEletricControlerApp().run()