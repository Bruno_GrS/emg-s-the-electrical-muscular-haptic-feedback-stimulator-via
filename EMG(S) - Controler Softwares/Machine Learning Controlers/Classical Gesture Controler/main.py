#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Machine Learning Controlers - Classical Gesture Controler
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

#Kivy imports
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy_garden.graph import MeshLinePlot
from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label 
from kivy.uix.spinner import Spinner
from kivy.uix.slider import Slider   
from kivy.uix.popup import Popup
from kivy.uix.image import Image
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
import kivy.properties as kyprops
from kivy.graphics import Line, InstructionGroup
import serial

#Imports Gerais 
import os
import random
import pandas as pd
import numpy as np
from collections import deque
from functools import partial
import threading
import ctypes

#machine Learning Imports
from sklearn.preprocessing import StandardScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.svm import SVC

from Emg_collector import EmgCollector # type: ignore
from DataHandler import DataHandler # type: ignore
import myo
   
class MenuScreen(Screen):
    loadfile = kyprops.ObjectProperty(None)
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()
    task = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(MenuScreen,self).__init__(**kwargs)
        self.DataH = DataHandler()
        self.EMS = serial.Serial()
        self.EMS.baudrate = 115200
        self.EMS.port = 'COM3'
        self.Class_boxes, self.Prepros_boxes, self.Feature_boxes, self.Classifier_boxes = list(), list(), list(), list()
        self.ig = InstructionGroup()
        x1,x2,x3,x4 = 25,430,600,1010 
        self.lines = [[x1, 627, x2, 627],[x3, 627, x4, 627],[x1, 519, x2, 519],[x3, 519, x4, 519],[x1, 412, x2, 412],[x3, 412, x4, 412],[x1, 234, x2, 234],[x3, 234, x4, 234],[x1, 150, x4, 150]]
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component\n"
        self.helpt = ("[size=20][b]Classical Classification Gesture Controler[/b][/size]\n\n"
        "[size=18][b]Introduction: [/b][/size]\n This Program is an example of a more complex methodology to generate a control signal via EMG, by "
        "using machine learning to perform pattern recognition and classification. "
        "This method of control allows for the identification of multiple diferent gestures, where each one coresponds to a specific stimulation pattern. "
        "More specificaly this example first loads a dataset of example gestures, each being a collection of N time-series of variable lenght. For each it then applies, in order, "
        "a variable number of preprossesing functions before using another variable number of functions(F) to extract features from the data. It`s goal being reducing the number of data points "
        "for each example from N*W to N*F, where W >> F. Lastly it uses this reduced representation to train a classification algoritm, in other words a mathematical function that is able to "
        "group examples from the same gesture while separating examples from diferent gestures. Whith the training compleat the program is able to start accepting new data, in real time, from the "
        "Myo Armband and output a classification result for it indicating wich of the trained gestures it most resembles."
        "\n\n[size=18][b]Dataset Selection: [/b][/size]\n"
        "Any dataset can be select as long as it is follows some guidelines. Consult the README.md for more information"
        "\n\n[size=18][b]Configuration Options: [/b][/size]\n"
        "This first window allows the user to select some parameters before training the classifer. To read more information about each move the mouse over each")
        Window.bind(mouse_pos=self.Check_Mouse)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component\n"
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about the program\n"
        if self.ids.B_path.collide_point(*y):
            self.text = "[size=18][b]This is the Select Dataset Button[/b][/size] \n\nSelecting this button opens up the file explorer so you can open the Dataset file for training\n"
        if self.ids.Lpath.collide_point(*y):
            self.text = "[size=18][b]This is shows the path to the current selected Dataset[/b][/size] \n\nIf no valid Dataset is selected no path is displayed\n"   
        if self.ids.B_classes.collide_point(*y):
            self.text = "[size=18][b]This is the Class Selection Box[/b][/size] \n\nAfter loading a valid Dataset use the Checkboxs that appear to select what Gestures(Classes) will be included in training\n"
        if self.ids.B_prepros.collide_point(*y):
            self.text = "[size=18][b]This is the Preprocessing Box[/b][/size] \n\nIn this box you can select any of 3 preprocessing functions\nAbsolute value calculation\nEnveloping of Data\nFrequency filtering"
        if self.ids.B_features.collide_point(*y):
            self.text = "[size=18][b]This is the Features Extration Box[/b][/size] \n\nIn this box you can select what feature extration functions will be used\nFor more information on each Consult the README.md\n"
        if self.ids.B_classifiers.collide_point(*y):
            self.text = "[size=18][b]This is the Classifier selection Box[/b][/size] \n\nIn this box you can select one of two classification algoritms\nFor more information on each Consult the README.md\n"
        if self.ids.B_Myo.collide_point(*y) or self.ids.B_MyoC.collide_point(*y):
            self.text = "[size=18][b]This informs if the Myo is connected[/b][/size] \n\nIf not connected it tries to initiate connection every 5s\n"
        if self.ids.B_EMS.collide_point(*y) or self.ids.B_EMSC.collide_point(*y):
            self.text = "[size=18][b]This informs if the EMS is connected[/b][/size] \n\nIf not connected it tries to initiate connection every 5s\n"
        if self.ids.B_next.collide_point(*y):
            self.text = "[size=18][b]This Confirms the Training parameters[/b][/size] \n\n After clicking the process of training will start. It may take a while depending on the size of the dataset\n"


    def on_enter(self, *args):
        Clock.schedule_once(lambda dt: self.Create_layout())

    def Create_layout(self):
        [self.ig.add(Line(points=c)) for c in self.lines]
        self.canvas.add(self.ig)
        self.Default = Label(text = "Classes: ...... ")
        self.ids.B_classes.add_widget(self.Default)
        for x,f in enumerate(self.DataH.prepross_all):
            self.ids.B_prepros.add_widget(Label(text = f))
            self.Prepros_boxes.append(CheckBox())
            self.ids.B_prepros.add_widget(self.Prepros_boxes[x])
        for x,f in enumerate(self.DataH.features_all):
            self.ids.B_features.add_widget(Label(text = f))
            self.Feature_boxes.append(CheckBox())
            self.ids.B_features.add_widget(self.Feature_boxes[x])
        for x,f in enumerate(self.DataH.clfs_avaliable):
            self.ids.B_classifiers.add_widget(Label(text = f))
            self.Classifier_boxes.append(CheckBox(group="Class"))
            self.ids.B_classifiers.add_widget(self.Classifier_boxes[x])
        self.Classifier_boxes[0].active = True
        self.Test_MyoConection()
        self.Test_EMSConection()   

    def Test_MyoConection(self):    
        try:
            Listener.Toggle_streaming()
            self.ids.B_Myo.text = "[color=#52ef00]MyoConnect Running, Armband Connected[/color]"
            self.ids.B_MyoC.active = True
        except:
            Clock.schedule_once(lambda dt: self.Test_MyoConection(),5)

    def Test_EMSConection(self):
        try:
            self.EMS.open()
            self.ids.B_EMS.text = "[color=#52ef00]EMS via Serial Connected and Listening[/color]"
            self.ids.B_EMSC.active = True
        except:
            Clock.schedule_once(lambda dt: self.Test_EMSConection(),5)
    
    def dismiss_popup(self):
        self._popup.dismiss()

    def show_load(self):
        print(os.path.dirname(__file__))
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup,start_path=os.path.dirname(os.path.abspath(__file__)))
        self._popup = Popup(title="Load file", content=content, size_hint=(0.9, 0.9),auto_dismiss=False)
        self._popup.open()

    def load(self, path, filename):
        if filename:
            self.dismiss_popup()
            if self.DataH.Load_dataset(filename[0]):
                content = Error(close=self.dismiss_popup,content = "Not Able to Load Data")
                self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
                self._popup.open()
            else:
                self.ids.Lpath.text = "...\\"+"\\".join(filename[0].split("\\")[-2:-1])+"\\"+filename[0].split("\\")[-1]
                self.Update_Page()
                self.ids.B_path.disabled = True
    
    def Update_Page(self):
        self.ids.B_classes.remove_widget(self.Default)
        for x,f in enumerate(np.unique(self.DataH.Raw_Data["Class"])):
            self.ids.B_classes.add_widget(Label(text = "Class: "+str(int(f))))
            self.Class_boxes.append(CheckBox())
            self.ids.B_classes.add_widget(self.Class_boxes[x])

    def Confirm_selections(self):
        if self.DataH.Dataset_Loaded:
            classes_selected = list()
            for x in np.unique(self.DataH.Raw_Data["Class"]):
                classes_selected.append(self.Class_boxes[int(x)].active)
            if sum(classes_selected) < 2:
                content = Error(close=self.dismiss_popup,content = "Select At Least 2 Classes")
                self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
                self._popup.open()
                return
            features_active = list()    
            for x,_ in enumerate(self.DataH.features_all):
                features_active.append(self.Feature_boxes[int(x)].active)
            if features_active.count(True) == 0:
                content = Error(close=self.dismiss_popup,content = "Select At Least 1 Feature")
                self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
                self._popup.open()
                return 
            # if  not self.ids.B_MyoC.active:
            #     content = Error(close=self.dismiss_popup,content = "MyoArmband not Connected")
            #     self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
            #     self._popup.open()
            #     return 
            # if  not self.ids.B_EMSC.active:
            #     content = Error(close=self.dismiss_popup,content = "EMS not Connected")
            #     self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
            #     self._popup.open()
            #     return
            #Start calculating/Training
            self.thread = threading.Thread(target=self.Calculate_all,args=(classes_selected,features_active))
            self.thread.start()
            self.task = "PreProssesing \n Should Not Take Long"
            self.pop = Popup(title="Preparing Classifier", content=Label(text=self.task),size_hint=(None, None), size=(400, 400),auto_dismiss=False)
            self.pop.open()
            self.clock = Clock.schedule_interval(lambda dt: self.Update_popup(),1)
        else:
            content = Error(close=self.dismiss_popup,content = "Load a Valid Dataset")
            self._popup = Popup(title="Error", content=content, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
            self._popup.open()
            return  

    def Calculate_all(self,classes_selected,features_active):
        self.DataH.Update_All_Selections((self.Prepros_boxes[0].active,self.Prepros_boxes[1].active),features_active)
        self.task = "Epoching Data \n Should Not Take Long"
        self.DataH.Epoch_Data()
        self.task = "Selecting Classes \n Should Not Take Long"
        self.DataH.Select_Classes(classes_selected)
        self.task = "PreProssesing \n Should Not Take Long"
        self.DataH.Preprosses_Data()
        self.task = "Extrating Features \n May Take a While"
        self.DataH.Feature_Data()
        self.task = "Standartizing \n Should Not Take Long"
        self.DataH.Standartize_Data()
        self.task = "PreProssesing \n May Take a While"
        self.DataH.Train_Classifier(self.Classifier_boxes[0])


    def Update_popup(self):
        if not self.thread.is_alive():
            self.pop.dismiss()
            self.clock.cancel()
            self.manager.current = "Classification"
    

class ClassificationScreen(Screen):
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(ClassificationScreen,self).__init__(**kwargs)
        self.plotC = [[0, 0.4470, 0.7410],[0.8500, 0.3250, 0.0980],[0.9290, 0.6940, 0.1250],[0.4940, 0.1840, 0.5560],[0.4660, 0.6740, 0.1880],[0.3010, 0.7450, 0.9330],[0.6350, 0.0780, 0.1840],[0.25, 0.25, 0.25]]
        self.Base_plot = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.lenPlot = 1000
        self.lenWindow = 100
        self.EMG = tuple(deque(maxlen=self.lenPlot) for _ in range(8))
        self.Class_window = tuple(deque(maxlen=self.lenWindow) for _ in range(8))
        [self.EMG[x].append(j) for i in range(self.lenPlot) for x,j in enumerate(np.zeros((8,1000))[:,i])]
        self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component\n"
        self.helpt = ("[size=20][b]Classical Classification Gesture Controler[/b][/size]\n\n"
        "In this Screen the Classifer was trained using the Dataset provided and now it is ready for online querry. "
        "The EMG Signal read by the Myo is preprossed (by the same functions selected in the training step) and ploted on the graph on the left. Every 0.1 second the last 100 points recived are "
        "feed to the trained classifier that returns one of the possible classes as an output. The corresponding image them flashes green until other classification result is reached. "
        "If the EMS system is connected and activated them the corresponding stimulation, for each class, is performed. The corresponding stimuation can be selected from a dropbox menu from "
        "a number of options.")
        self.thread = threading.Thread(target=self.Online_Classification)
        self.time_passed = True
        Window.bind(mouse_pos=self.Check_Mouse)

    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component\n"
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about current page you are in\n"

    def on_pre_enter(self, *args):
        self.DataH = self.manager.get_screen("Menu").DataH
        self.EMS = self.manager.get_screen("Menu").EMS
        Listener.Clear_queue()
        [self.ids.graph.add_plot(self.Base_plot[x]) for x,_ in enumerate(self.Base_plot)]
        self.ValueClock = Clock.schedule_interval(lambda dt: self.Get_value(), 0.1)

    def on_enter(self, *args):
        Clock.schedule_once(lambda dt: self.Create_layout())
        self.Class_imgs,self.Spiners,self.Intensities = list(),list(),list()

    def Create_layout(self):
        for x,f in enumerate(np.unique(self.DataH.Epoched_Classes)):
            box = BoxLayout(size_hint=(0.95,0.8))
            name = "Images\\" + str(int(f+1)) + ".PNG"
            self.Class_imgs.append(Image(source=name,size_hint=(0.95,0.8)))
            box.add_widget(self.Class_imgs[x])
            self.ids.B_IMG.add_widget(box)
            box = BoxLayout(size_hint=(0.95,0.8))
            self.Spiners.append(Spinner(text="Channel 1",values=["Channel 1","Channel 2"],size_hint=(0.95,0.8)))
            box.add_widget(self.Spiners[x])
            self.ids.B_IMG.add_widget(box)
            box = BoxLayout(size_hint=(0.95,0.8),orientation="vertical")
            self.Intensities.append(Slider(min=0,max=100,value=0,step=1,orientation="horizontal",value_track=True,value_track_color=[1,0,0,1],size_hint=(0.92,0.8)))
            box.add_widget(Label(text="Intensity Controler 0% - 100%"))
            box.add_widget(self.Intensities[x])
            self.ids.B_IMG.add_widget(box)

    def Get_value(self):
        data = Listener.Send_data(25)
        if data is not None:
            [self.Class_window[x].append(j) for i in range(data.shape[1]) for x,j in enumerate(data[:,i])]
            [self.EMG[x].append(j) for i in range(data.shape[1]) for x,j in enumerate(data[:,i])]
            for x in range(data.shape[0]):
                self.Base_plot[x].points = [(i, j) for i, j in enumerate(self.EMG[x])]
            if not self.thread.is_alive():
                if self.time_passed:
                    self.thread = threading.Thread(target=self.Online_Classification)
                    self.time_passed = False
                    Clock.schedule_once(lambda dt: self.timer(),1)
                    self.thread.start()

    def Online_Classification(self):
        if len(self.Class_window[0]) > (self.lenWindow-1):
            data = np.zeros((8,self.lenWindow))
            for x in range(data.shape[0]):
                data[x,:] = np.array(list(self.Class_window[x]))
            data = data.T
            data = self.DataH.Preprosses_Test(data)
            features = self.DataH.Calculate_Features_test(data)
            features = self.DataH.Standartize_Test(features)
            prediction =  self.DataH.Test_Classifier(features)
            Clock.schedule_once(partial(self.update_image,prediction))
            Clock.squedule_once(partial(self.Activate_EMS,prediction))

    def Timer(self):
        self.time_passed = True

    def update_image(self,prediction, *largs):
        for imgs in self.Class_imgs:
            imgs.color = [1, 1, 1, 1]
        self.Class_imgs[int(prediction)].color = [0, 1, 0, 1]

    def Activate_EMS(self,prediction,*largs):
        channel = int(self.Spiners[int(prediction)].text[-1]) - 1
        intensity = self.Intensities[int(prediction)].value
        comand = "C"+ str(channel) + "I"+ str(intensity) + "T0100G\n"
        self.EMS.write(comand.encode())
        self.EMS.reset_output_buffer()

    def Stop_EMS(self):
        Clock.unschedule(self.Online_Classification())
        Clock.unschedule(self.Activate_EMS)
        for x,f in enumerate(np.unique(self.DataH.Epoched_Classes)):
            self.Intensities[x].value = 0
        


#Suport Functions

class Error(FloatLayout):
    close = kyprops.ObjectProperty(None)
    content = kyprops.StringProperty(None)

class LoadDialog(FloatLayout):
    load = kyprops.ObjectProperty(None)
    cancel = kyprops.ObjectProperty(None)
    start_path = kyprops.StringProperty(None)

class Manager(ScreenManager):
    pass

# kv = Builder.load_file("MyoEletricControler.kv")
class MyoEletricControlerApp(App):
    def build(self):
        Window.size = (1280,720)
        m = Manager(transition=NoTransition())
        return m



if __name__ == '__main__':
    myo.init()
    try:
        hub = myo.Hub()
    except :
        ctypes.windll.user32.MessageBoxW(0, "Is Myo Connect running?\nOpen MyoConnect before running", "Unable to connect to Myo Connect", 0)
        quit()
    Listener = EmgCollector(100) #Creates Object that listens to the Armband
    with hub.run_in_background(Listener.on_event): # The whole window is run inside the Listerner Context
        MyoEletricControlerApp().run()