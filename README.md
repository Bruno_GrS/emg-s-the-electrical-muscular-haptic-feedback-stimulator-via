# **Myoelectric Simulator**

## An Electromyography system that control a toolkit for force feedback using Electric Muscular Stimulation

[Before using this the programs in this repository follow to the instructions on the main page](https://bitbucket.org/Bruno_GrS/myo_estim/wiki/Home)

### **Disclaimer**

THIS REPOSITORY IS FOR INFORMATIONAL PURPOSES ONLY. ALL THE INFORMATION AND CODE PROVIDED IS PROVIDED "AS-IS" AND WITH NO WARRANTIES. NO EXPRESS OR IMPLIED WARRANTIES OF ANY TYPE, INCLUDING FOR EXAMPLE IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, ARE MADE. EMG AND EMS CARRY RISK KNOW AND UNKNOW TO THE HUMAN BODY. THIS PROJECT AND ITS PARTS ARE NOT PROOFED FOR MEDICAL USE. IF YOU NOT KNOW WHAT YOU ARE DOING, DO NOT USE THIS PROJECT.

### **Safety Information**

**WARNING! USES OF EMG and EMS CAN LEAD TO PAIN, INJURY OR TO DEATH REGARDING KNOWN AND UNKNOWN DISEASE OR WRONG USAGE.**

Our repository does not contain any information on building any hardware related to EMG or EMS. Nonetheless it uses and Controls both and EMG and an EMS system and there exists many, know and unknow, risks on using both. WE ARE NOT RESPONSIBLE FOR ANY SIDE EFFECTS. The programs where are to be only used as reference and may or may NOT control both systems correctly. All usage is done at you own risk. If you are unsure how to use it, do NOT use it. Check Both Myo Armbands and the [Letyourbodymove](https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home/#markdown-header-safety-issues) Safety Instructions before any use of both.

## Autors

Bruno Grandi Sgambato - brunog.sgambato at gmail dot com

## Copywrite

* Copyright by Bruno Grandi Sgambato in 2020 - [Id Lattes](http://lattes.cnpq.br/2759619062389430)
* Copyright by Gabriela Castellano in 2020 – [Id Lattes](http://lattes.cnpq.br/5993832413749292), [Orcid](https://orcid.org/0000-0002-5927-487X)

## License

MIT License

Copyright (c) 2020 Bruno Grandi Sgambato

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Some Parts of this Work modifications on a work Licensed (MIT license) and Copyrighted by Max Pfeiffer and Tim Dünte and marked accordingly.