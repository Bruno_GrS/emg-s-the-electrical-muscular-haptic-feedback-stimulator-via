#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Intensity Controlers - On Off Threshold Controler
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

#Kivy imports
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.boxlayout import BoxLayout
from kivy_garden.graph import MeshLinePlot
from kivy.clock import Clock
from kivy.core.window import Window
import kivy.properties as kyprops
#Imports Gerais 
import numpy as np
from collections import deque
import serial
import ctypes
#Imports Especificos
from Emg_collector import EmgCollector # type: ignore
import myo


class Logic(BoxLayout):

    #StringProperty definitons
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self,**kwargs):
        super(Logic, self).__init__(**kwargs)
        self.EMS = serial.Serial()
        self.EMS.baudrate = 115200
        self.EMS.port = 'COM3'
        self.Threshold = None
        self.plotC = [1,1,1,1]
        self.Base_plot = MeshLinePlot(color=self.plotC)
        self.ids.graph.add_plot(self.Base_plot)
        self.lenPlot = 1000
        self.EMG = deque(maxlen=self.lenPlot)
        self.time = 0
        self.Connected = False
        self.filer = (" \n")
        self.text = "This is the Tips Box \nMove your mouse arround to see Tips about each interactible component" + self.filer
        self.helpt = ("[size=20][b]On/Off Threshold Controler[/b][/size]\n\n"
        "[size=18][b]Introduction: [/b][/size]\n This program is shows an example of one of the simplest way a sEMG Signal "
        "can be used to generate a control signal called a On/Off controler via Threshold. "
        "This controler allows, as the name implies, two possible states. A On state and a Off state, the systems decides betwen each state "
        "by using a Threshold. A Threshold is a predertermided value, when the EMG value is bigger than the Threshold the system is on the On state, "
        "on the other hand when the value is smaler than the Threshold the system is in the off state.\n\n[size=18][b]EMG Signal: [/b][/size]\n"
        "When the Armband is Positioned on the arm its sensors each mesures, over time, a composed signal from the nearts muscles, in other other eight diferent signals."
        "However this program takes a diferent aproach looking at the SUM of all sensors for each time sample. In other other the signal graphed does not represent any especcific"
        "Muscular contration but is a composition of how much \"Force\" is being produced all arround the muscles on the forearm.\n\n[size=18][b]Threshold: [/b][/size]\n"
        "When a Threshold is selected a Green line will be draw on the Graph an whenever the white EMG signal read is bigger than it stimulation will be sent. "
        "This Code in Specifc will send a full intensity stimulation (100% intensity) when the Threshold is meet. So We suggest starting with very low intensities in the signal generator")
        Window.bind(mouse_pos=self.Check_Mouse)
    
    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component" + self.filer
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about the program" + self.filer
        if self.ids.graph.collide_point(*y):
            self.text = "[size=18][b]This is the Main Graph[/b][/size] \n\nThe EMG signal from the Myo is ploted here thoghether with the Treshold selected" + self.filer
        if self.ids.B_st.collide_point(*y):
            self.text = "[size=18][b]This is the Start/Stop Button[/b][/size] \n\nClicking this button starts/stops the EMG streaming and ploting on the main Graph" + self.filer
        if self.ids.B_ems.collide_point(*y):
            self.text = "[size=18][b]This is the EMS conection Button[/b][/size] \n\nClicking this button connects the program to the EMS estimulator, and if the threshold its activated starts sending signals" + self.filer
        if self.ids.B_Thr.collide_point(*y):
            self.text = "[size=18][b]This is the Threshold Button[/b][/size] \n\nClicking this button activates a Threshold, if the sistems is connected to the EMS it will start sending estimulations" + self.filer
        if self.ids.B_Thrup.collide_point(*y):
            self.text = "[size=18][b]This is the Threshold Up Button[/b][/size] \n\nClicking this button moves the Threshold up by 10 points" + self.filer
        if self.ids.B_Thrdw.collide_point(*y):
            self.text = "[size=18][b]This is the Threshold Down Button[/b][/size] \n\nClicking this button moves the Threshold down by 10 points" + self.filer

    def Toggle_Emg(self):
        if Listener.streaming == False:
            Listener.Toggle_streaming()
            if not Listener.Toggle_streaming():
                self.Show_Error("Myo Armband Not Connected.\n\nMake sure the Myo is not asleep", "Myo not Connected")
                return
            self.Myo_clock = Clock.schedule_interval(lambda dt: self.get_value(), 0.1)
            self.ids.B_Thr.disabled = False
            self.ids.B_st.text = "[color=#52ef00]Start[/color]/Stop"  
        else:
            Listener.Toggle_streaming()
            self.Myo_clock.cancel()
            if self.Threshold is not None:
                self.set_threshold()
            self.ids.B_Thr.disabled = True
            self.ids.B_st.text = "Start/[color=#e41e00]Stop[/color]"  
            self.EMG.clear()

    def start_serial(self):
        if not self.Connected:
            try:
                self.EMS.open()
            except:
                self.Show_Error("EMG(S) serial not Avaliable.\n\nMake sure the serial port is open and free", "EMG(S) not Connected")
            if self.EMS.is_open == True:
                self.Connected = True
                self.ids.B_ems.text = "[color=#52ef00]Connected[/color]"
                self.EMS.reset_input_buffer()
        else:
            self.EMS.reset_input_buffer()
            self.EMS.close()
            self.ids.B_ems.text = "Connect to Estimulator"
            self.Connected = False
            
    def Show_Error(self,text1,text2):
        self.box_popup = BoxLayout(orientation = 'horizontal')
        self.box_popup.add_widget(Label(text = text1))
        self.box_popup.add_widget(Button(text = "Ok", on_press = lambda *args: self.popup_exit.dismiss(), size_hint = (0.215, 0.075)))
        self.popup_exit = Popup(title = text2, content = self.box_popup,size_hint=(None, None), size=(400, 400), auto_dismiss = True)  
        self.popup_exit.open()

    def set_threshold(self):
        if self.Threshold is None:
            self.Threshold = 5
            self.Threshold_plot = MeshLinePlot(color=[0,0.937,0.122,1])
            self.ids.graph.add_plot(self.Threshold_plot)
            self.Threshold_plot.points = [(i, j) for i, j in enumerate(np.full((self.lenPlot),self.Threshold))]
            self.ids.B_Thrup.disabled = self.ids.B_Thrdw.disabled = False
            self.Thresh_clock = Clock.schedule_interval(lambda dt: self.get_thresh(), 1)
        else:
            self.Threshold = None
            self.ids.B_Thrup.disabled = self.ids.B_Thrdw.disabled = True
            self.Threshold_plot.points = [(i, j) for i, j in enumerate(np.full(1,0))]
            self.Thresh_clock.cancel()

    def up_threshold(self):
        self.Threshold = min([self.Threshold + 10, 255])
        self.Threshold_plot.points = [(i, j) for i, j in enumerate(np.full((self.lenPlot),self.Threshold))]

    def down_threshold(self):
        self.Threshold = max([self.Threshold - 10, 1])
        self.Threshold_plot.points = [(i, j) for i, j in enumerate(np.full((self.lenPlot),self.Threshold))]

    def get_thresh(self):
        if self.EMS.is_open:
            self.EMS.reset_input_buffer()
            if self.Threshold < self.EMG[-1]:
                value = "I100T1000G\n"
                comand = "C0" + value
                self.EMS.write(comand.encode())
                comand = "C1" + value
                self.EMS.write(comand.encode())
            else:
                self.EMS.reset_output_buffer()

    def get_value(self):
        data = Listener.Send_data_Enveloped()
        if data is not None:
            [self.EMG.append(data[i]) for i in range(len(data))]
            self.Base_plot.points = [(i, j) for i, j in enumerate(self.EMG)]


class MyoEletricControlerApp(App):
    def build(self):
        Window.size = (1280,720)
        return Logic() 

if __name__ == '__main__':
    myo.init()
    try:
        hub = myo.Hub()
    except :
        ctypes.windll.user32.MessageBoxW(0, "Is Myo Connect running?\nOpen MyoConnect before running", "Unable to connect to Myo Connect", 0)
        quit()
    Listener = EmgCollector(100) #Creates Object that listens to the Armband
    with hub.run_in_background(Listener.on_event): # The whole window is run inside the Listerner Context
        MyoEletricControlerApp().run()