# Arduino Software

## What is this

This folder contains two softwares made for the Arduino to control the Eletrical Muscular Estimuation. This programs are suposed to be used with our version of the [LetYoutBodyMove](https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home) toolkit.

Before using this make sure to follow the [Getting Started](https://bitbucket.org/Bruno_GrS/myo_estim/wiki/Home#markdown-header-Getting-Started) guide.

## Overview

One of the avaliable software called Standalone_Controler_for_the_Arduino is able to connect itself to a Myo Armband and use its sEMG readings as a input to a Threshold controler that generaties a On Off EMS stimulation.

The other is called Arduino_Controler controls the EMS stimulation via a series of Comands that need to send via a Serial Connection.

### Comands and Serial

The comands are a simple 8 characters strings followed by a line break. They inform the channel, the intensiry and the lenght of the estimuation. The table bellow explains them.

| *Commands*   | *Values*      | *Description*             | *Example*                  |
|------------|-------------|-------------------------|--------------------------|
| Channel    | 0 - 1       | Channel to be Activated | C0 - Activates channel 0 |
| Intensity  | 000 - 001   | Intensity (%)           | I50 - With Intensity 50% |
| Duration   | 0001 - 9999 | Duration (ms)           | T100 - For 1s (1000ms)   |
| Activation | x           | End of Command          | G - Ends the Command     |

The Serial comunication works via the USB cable. The defaut baud rate is 115200.
