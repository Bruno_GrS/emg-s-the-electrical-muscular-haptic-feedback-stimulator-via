# Proportional Controler via Multiple Threholds

## Description

For cases like Eletrical Estimulation where you may not only desire your control signal to indicate an On-Off but an degree of activation
The proportional controler is the simplest way to achive this. While the main idea can be implemented in a variety of ways this one is done with a controlable 2 step threshold.
With the envelop calculated and a 2 step threshold determined the program determines for every time point where the EMG signal is.
If its lower than the 1 step of the treshold it is Off, if its between the first and second step the intensity is decided as a mesure to exactly where it its and if its higher than the 2 step it is capped at 100% intensity.

## Dependencies

* kivy
* kivy_garden
* pandas
* numpy
* pyserial
* myo-python
