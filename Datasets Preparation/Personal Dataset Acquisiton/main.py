#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Personal Dataset Acquisition
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

#Kivy imports
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy_garden.graph import MeshLinePlot
from kivy.uix.label import Label
from kivy.uix.button import Button    
from kivy.uix.popup import Popup
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen
import kivy.properties as kyprops
from kivy.uix.image import Image

#Imports Gerais 
import pandas as pd
import numpy as np
from collections import deque
from functools import partial
import ctypes
#Imports Especificos
from Emg_collector import EmgCollector # type: ignore
import myo


class FirstScreen(Screen):
    #StringProperty definitons
    helpt = kyprops.StringProperty()
    text = kyprops.StringProperty()

    def __init__(self, **kwargs):
        super(FirstScreen,self).__init__(**kwargs)
        self.plotC = [[0, 0.4470, 0.7410],[0.8500, 0.3250, 0.0980],[0.9290, 0.6940, 0.1250],[0.4940, 0.1840, 0.5560],[0.4660, 0.6740, 0.1880],[0.3010, 0.7450, 0.9330],[0.6350, 0.0780, 0.1840],[0.25, 0.25, 0.25]]
        self.Base_plot = tuple(MeshLinePlot(color=c) for c in self.plotC)
        self.lenPlot = 1000
        self.EMG = tuple(deque(maxlen=self.lenPlot) for _ in range(8))
        [self.EMG[x].append(j) for i in range(self.lenPlot) for x,j in enumerate(np.zeros((8,1000))[:,i])]
        self.text = "This is the Tips Box \nMove your mouse arround to see Tips about each interactible component\n"
        self.helpt = ("[size=20][b]Personal Dataset Construction[/b][/size]\n\n"
        "This Small program guides a user in making a peronal dataset of contractions, using the Myo Armband, to use in other programs."
        "To do so: \n 1) Use this screen to make sure the Myo is positioned and being read correctly using the graph\n"
        "2) Select how many gestures you will be perfoming. You will se the images of each.\n"
        "3) Select how many tries for each gesture you will be doing.\n"
        "4) Prepare yourself and select the \"Starting Recording\" button.\nEach image will be show for 4 seconds with one second breaks between them.")
        self.gestures_lists = ['2 Gestures', '3 Gestures', '4 Gestures', '5 Gestures', '6 Gestures', '7 Gestures', '8 Gestures', '9 Gestures']
        Window.bind(mouse_pos=self.Check_Mouse)
        
    def Check_Mouse(self,instance,y):
        if self.ids.tips.collide_point(*y):
            self.text = "[size=18][b]This is the Tips Box[/b][/size] \n\nMove your mouse arround to see Tips about each interactible component\n"
        if self.ids.help.collide_point(*y):
            self.text = "[size=18][b]This is the Help Box[/b][/size] \n\nIt describes the basic workings and informations about the program\n"
        if self.ids.graph.collide_point(*y):
            self.text = "[size=18][b]This is the Graph[/b][/size] \n\nIf the graph does not show the EMG signal the Myo is not connected properly. Restart the program or check the Myo\n"
        if self.ids.S_gestures.collide_point(*y):
            self.text = "[size=18][b]This is the Gesutes Selector[/b][/size] \n\nUse it to select how many gestures you will be performing\n"
        if self.ids.B_gestures.collide_point(*y):
            self.text = "[size=18][b]These are the Gesture Images[/b][/size] \n\nTo use personalized image substitute the ones in the \\labels_img folder\n"
        if self.ids.T_slider.collide_point(*y) or self.ids.T_label.collide_point(*y):
            self.text = "[size=18][b]This is the Slider[/b][/size] \n\nUse this slider to select the number of tries for each gesture. Take care to not select to much as it will take long to record\n"
        if self.ids.B_st.collide_point(*y):
            self.text = "[size=18][b]This is the Start Recording Button[/b][/size] \n\nWhen you select it will start the recording of gestures.\n"

    def on_pre_enter(self, *args):
        Listener.Clear_queue()
        self.ValueClock = Clock.schedule_interval(lambda dt: self.Get_value(), 0.1)

    def on_enter(self, *args):
        Clock.schedule_once(lambda dt: self.Prepare_layout())

    def Prepare_layout(self):
        self.ngestures = 3
        [self.ids.graph.add_plot(self.Base_plot[x]) for x,_ in enumerate(self.Base_plot)]
        self.ntries = 10
        self.ids.T_slider.bind(value=partial(self.slider_change, self.ids.T_slider))
        self.Class_imgs = list()
        Clock.schedule_once(lambda dt: self.Create_layout())

    def Create_layout(self):
        if self.Class_imgs:
            for gesture in self.Class_imgs:
                self.ids.B_gestures.remove_widget(gesture)
        self.Class_imgs.clear()
        display = ['1 Gestures']
        for text in self.gestures_lists:
            if text == self.ids.S_gestures.text:
                display.append(text)
                break
            else:
                display.append(text)
        for x,gesture in enumerate(display):
            path = "labels_img\\" + gesture[0] + ".PNG"
            self.Class_imgs.append(Image(source=path,size=(100,100)))
            self.ids.B_gestures.add_widget(self.Class_imgs[x])
        self.ngestures = int(display[-1][0])  

    def slider_change(self, s, instance, value):
        self.ids.T_label.text = "Tries per Gesture: " + str(int(value))
        self.ntries = int(value)

    def Get_value(self):
        if not Listener.Check_disconnect():
            data = Listener.Send_data(25)
            if data is not None:
                [self.EMG[x].append(j) for i in range(data.shape[1]) for x,j in enumerate(data[:,i])]
                self.Plot_onGraph()
        else:
            [self.EMG[x].append(j) for i in range(self.lenPlot) for x,j in enumerate(np.zeros((8,1000))[:,i])]
            self.Plot_onGraph()

    def Plot_onGraph(self):
        for x in range(len(self.EMG)):
            self.Base_plot[x].points = [(i, j) for i, j in enumerate(self.EMG[x])]


    def Start_Recording_Emg(self):
        self.manager.current = "Data"

class DataScreen(Screen):

    #StringProperty definitons
    instructions = kyprops.StringProperty()

    def __init__(self, **kwargs):
        super(DataScreen,self).__init__(**kwargs)
        self.instructions = "The First Gesture will appear in [b]5[/b] seconds"
        self.col = ["emg0","emg1","emg2","emg3","emg4","emg5","emg6","emg7","Class","Subject"]
        self.df = pd.DataFrame(columns=self.col)

    def on_pre_enter(self, *args):
        self.ngestures = self.manager.get_screen("First").ngestures
        self.ntries = self.manager.get_screen("First").ntries
        self.order = list()
        for x in np.arange(1,self.ngestures+1,1):
            self.order.extend(np.full(self.ntries,x))
        self.order = list(np.random.permutation(self.order))
        self.counter = 4
        self.clock = Clock.schedule_once(lambda dt: self.Preparation(),1)

    def Preparation(self):
        self.instructions = "The First Gesture will appear in [b]"+str(self.counter)+"[/b] seconds"
        self.counter = self.counter - 1
        if self.counter == 0:
            self.clock = Clock.schedule_interval(lambda dt: self.Gesture_Update(),1)
            self.counter = 0
        else:
            self.clock = Clock.schedule_once(lambda dt: self.Preparation(),1)
        
    def Gesture_Update(self):
        if self.counter == 0:
            Listener.Clear_queue()
            if self.order:
                self.last_gesture = self.order.pop()
            else:
                self.clock.cancel()
                Clock.schedule_once(lambda dt: self.End_Program(),1)
            self.counter = 6
        if self.counter > 2:
            path = "labels_img\\" + str(self.last_gesture) + ".PNG"
            self.ids.IMG.source = path
            self.instructions = "Perform and Hold the Gesture " + str(self.last_gesture) + " for [b]"+str(self.counter-2)+"[/b] Seconds"
        if self.counter < 3 and self.counter > 0:
            if self.counter == 2:
                data = Listener.Send_data(-1).T
                data = np.concatenate((data,np.full((data.shape[0],1),self.last_gesture)),axis=1)
                data = np.concatenate((data,np.full((data.shape[0],1),99)),axis=1)
                self.df = self.df.append(pd.DataFrame(data,columns=self.col),ignore_index=True)
            self.ids.IMG.source = "labels_img\\R.PNG"
            self.instructions = "Release the Gesture and Rest. The next one will show up in [b]"+str(self.counter) +"[/b] seconds"
        self.counter = self.counter - 1


    def End_Program(self):
        self.box_popup = BoxLayout(orientation = 'horizontal')
        self.box_popup.add_widget(Label(text = "Dataset Captured"))
        self.box_popup.add_widget(Button(text = "Exit", on_press = lambda *args: self.popup_exit.dismiss(), size_hint = (0.215, 0.075)))
        self.popup_exit = Popup(title = "The dataset was saved as Personal_EMG_Data in this folder", content = self.box_popup,size_hint=(None, None), size=(400, 400), auto_dismiss = True)  
        self.popup_exit.open()
        self.df.to_csv("Personal_EMG_Data")
        App.get_running_app().stop()

class Manager(ScreenManager):
    pass

class AssistentApp(App):
    def build(self):
        Window.size = (1280,720)
        m = Manager()
        return m

if __name__ == '__main__':
    myo.init()
    try:
        hub = myo.Hub()
    except :
        ctypes.windll.user32.MessageBoxW(0, "Is Myo Connect running?\nOpen MyoConnect before running", "Unable to connect to Myo Connect", 0)
        quit()
    Listener = EmgCollector(1000) #Creates Object that listens to the Armband
    with hub.run_in_background(Listener.on_event): # The whole window is run inside the Listerner Context
        AssistentApp().run() 