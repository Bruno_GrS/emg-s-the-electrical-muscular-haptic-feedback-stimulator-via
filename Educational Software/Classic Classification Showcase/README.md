# Classic Classificatory Showcase - Kivy Based - Windows Version

The Goal of this code is to provide an educational basis, so an interested user can learn and experience the pipeline of a Classical Classification Machine Learning experiment. It uses the Gesture Classification as an example to showcase how each part of the pipeline works in real time. It allows the user to choose different methods on each step of the process and for him to see how they affect the signals, and the finals results the model can achieve.

To Use this Program the user Needs to go to xxx, Download and process one of the suggested datasets and copy the resulting file in the /Dataset folder. We suggest the PINCH dataset as it deals with 3 clear gestures (Rock, Paper and Scissors) making the example simpler and more educative.

You can substitute the images on the Image folder. Make sure the names used on the new ones are the same.
