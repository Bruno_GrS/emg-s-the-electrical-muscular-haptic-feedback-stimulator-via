#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Educational Software for Classic Machine Learning Methods
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

import numpy as np
import os
import pandas as pd
from collections import deque
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from scipy import signal


class Data_Handler():
    def __init__(self,**kwargs):
        self.features_all = ["Root_mean_square","Mean_Absolute_Value","Slope_Sign_Change","Waveform_length","Zero_Crossings"]
        self.features_activated = list()
        self.features_funcs = {"Root_mean_square": self.Root_mean_square,     
                                "Mean_Absolute_Value": self.Mean_Absolute_Value,
                                "Slope_Sign_Change": self.Slope_Sign_Change,
                                "Waveform_length": self.Waveform_length,
                                "Zero_Crossings": self.Zero_Crossings}
        self.features_short = {"Root_mean_square": "RMS",     
                                "Mean_Absolute_Value": "MAV",
                                "Slope_Sign_Change": "SSC",
                                "Waveform_length": "WL",
                                "Zero_Crossings": "ZC"}
        self.TestFeats = deque()
        self.Dataset_Loaded = False
        self.percentage = [0,1]
    
    def Load_dataset(self,path):
        try:
            self.Raw_Data = pd.read_csv(path,index_col=0)
            if  "Class" in self.Raw_Data.columns:
                self.Raw_Data["Class"] -= 1 # arruma para começar em 0
                self.Raw_Data = self.Raw_Data.iloc[0:10000] #For Test Purposes-------------
                self.Dataset_Loaded = True
                return not self.Dataset_Loaded
            else:
                return not self.Dataset_Loaded
        except:
            return not self.Dataset_Loaded

    def Epoch_Data(self):
        self.Epoched_data,self.Epoched_Classes = list(), list()
        count = 0
        for x in range(self.Raw_Data.shape[0]):
            if self.Raw_Data["Class"][count] != self.Raw_Data["Class"][x]:
                self.Epoched_data.append(self.Raw_Data.loc[count:x-1])
                self.Epoched_Classes.append(self.Raw_Data["Class"][count])
                count = x

    def Select_Classes(self,classes_selected):
        temp = list()
        for x,data in enumerate(self.Epoched_data):
            if classes_selected[int(data["Class"].iloc[0])]:
                temp.append(self.Epoched_data[x])
        self.Epoched_data = temp

    def Preprosses_Data(self,ab,env,filt_bp,filt_bs,freqs_bp,freqs_bs):
        for x,data in enumerate(self.Epoched_data):
            data = np.array(data.drop(columns=["Subject","Class"]))
            if ab:
                data = np.absolute(data)
            if env:
                data = self.Envelop_Data(data,100)
            if filt_bp:
                data = self.Butter_filter(data,freqs_bp,'bandpass')
            if filt_bs:
                data = self.Butter_filter(data,freqs_bs,'bandstop')
            (self.Epoched_data[x]).loc[:,'emg0':'emg7'] = data

    def Feature_Data(self):
        self.Featured_Data = np.empty(((len(self.features_activated)*8)+2,0))
        self.percentage[0] = len(self.Epoched_data)
        count = 0
        for x,data in enumerate(self.Epoched_data):
            temp = np.array([data.iloc[0]["Class"],data.iloc[0]["Subject"]])
            data = data.drop(columns=["Subject","Class"])
            for elem in self.features_activated:
                t = self.Call_funcs(elem,data)
                temp = np.append(temp,t)     
            self.Featured_Data = np.append(self.Featured_Data,np.expand_dims(temp,axis=1),axis=1)
            count = x
            self.percentage[1] = count
        self.Standartize_Data()
    
    def Standartize_Data(self):
        scaler = StandardScaler()
        self.Featured_Data[2:,:] = scaler.fit_transform(self.Featured_Data[2:,:].T).T

    #======== Funcs to calculate examples for each part of the classification process ============

    def Calculate_Example(self,data):
        temp = np.empty((8,0))
        for elem in self.features_all:
            t = np.array(self.Call_funcs(elem,data))
            temp = np.append(temp,np.expand_dims(t,axis=1),axis=1) 
        return temp.T

    def PCA_Example(self,data):
        pca = PCA(n_components=2, svd_solver='full')
        return pca.fit_transform(data)

    def Update_selected(self,activated):
        self.features_activated.clear()
        for x,y in enumerate(self.features_all):
            if activated[x]:
                self.features_activated.append(y) 
    
    def Calculate_Features_test(self,test):
        # test = pd.DataFrame(data=test)
        temp = np.empty(0)
        for elem in self.features_activated:
            t = self.Call_funcs(elem,test)
            temp = np.append(temp,np.array(t))
        self.TestFeats.append(temp)


    #======== Suport Functions ============

    def Call_funcs(self,elem,data):    
        func_to_call = self.features_funcs[elem]
        result = func_to_call(data)
        return result

    def Butter_filter(self,data,filt,types):
        sos = signal.butter(14, filt, types, fs=200.1, output='sos')
        for x in range(data.shape[0]): 
            data[x] = signal.sosfilt(sos, data[x])
        return data


    def Envelop_Data(self,EMG,W):
        box = np.ones((W,))/W
        conv =  np.array([np.convolve(EMG[:,x], box, mode='same') for x in range(EMG.shape[1])])
        return conv.T

    def Root_mean_square(self,data_EMG): # Hassan, H. F., Abou-Loukh, S. J., & Ibraheem, I. K. (2019)
        rms = []
        for x in range(8):
            data = data_EMG.iloc[:,x]
            data = data ** 2
            su = data.sum()
            su = su/len(data)
            su = np.sqrt(su)
            rms.append(su)
        return rms
    
    def Mean_Absolute_Value(self,data_EMG): # Hassan, H. F., Abou-Loukh, S. J., & Ibraheem, I. K. (2019)
        mav = []
        for x in range(8):
            data = np.abs(data_EMG.iloc[:,x])
            su = data.sum()
            su = su/len(data)
            mav.append(su)
        return mav
        
    def Slope_Sign_Change(self,data_EMG): # Hassan, H. F., Abou-Loukh, S. J., & Ibraheem, I. K. (2019)
        ssc = []
        for x in range(8):
            data = data_EMG.iloc[:,x]
            to = 0
            for i in range(1,len(data)-1):
                su = (data.iloc[i] - data.iloc[i-1])*(data.iloc[i] - data.iloc[1+1])
                to = to + np.abs(su)
            ssc.append(to)
        return ssc

    def Waveform_length(self,data_EMG): # Hassan, H. F., Abou-Loukh, S. J., & Ibraheem, I. K. (2019)
        wl = []
        for x in range(8):
            data = data_EMG.iloc[:,x]
            to = 0
            for i in range(len(data)-1):
                su = data.iloc[i+1] - data.iloc[i]
                to = to + np.abs(su)
            wl.append(to)
        return wl

    def Zero_Crossings(self,data_EMG):
        zc = []
        for x in range(8):
            data = data_EMG.iloc[:,x]
            to = 0
            for i in range(len(data)-1):
                if((data.iloc[i]*data.iloc[i+1]) < 0 ):
                    sng = 1
                else:
                    sng = 0
                to = to + sng
            zc.append(to)
        return zc