'''

@author: dan
'''


from graph_plots.MGraph.extras_plots.graph import Graph
from graph_plots.MGraph.extras_plots.scatterPlot2D import ScatterPlot

import itertools
from kivy.utils import get_color_from_hex as rgb
import numpy as np
from graph_plots.fwidgets.f_boxlayout import FBoxLayout


class Scatter2DGraph(FBoxLayout):

    def __init__(self, **kwargs):
        super(Scatter2DGraph, self).__init__(**kwargs)

        self.orientation = "vertical"

        self._colors = itertools.cycle([rgb('8e44ad'), rgb('b7ff00'), rgb('DB0A5B'),
                                        rgb('C8F7C5'), rgb(
                                            '34495e'), rgb('f1c40f'),
                                        rgb('6C7A89'), rgb(
                                            'BFBFBF'), rgb('00CCFF'),
                                        rgb('e67e22')
                                        ])

        self._graph_theme = {
            'label_options': {
                'color': rgb('99ccff'),  # color of tick labels and titles
                'bold': True},
            # 3B3C3D back ground color of canvas 738ffe - 3B3C3D
            'background_color': rgb('000000'),
            'tick_color': rgb('5588ff'),  # ticks and grid
            'border_color': rgb('5588ff')}  # border drawn around each graph

        self._agraph = None

    def clear_all(self):
        if self._agraph is not None:
            for plot in self._agraph.plots:
                self._agraph.remove_plot(plot)

    def plotScatter(self, X, Y,x_max,x_min,y_max,y_min,collors,xlabel,ylabel):
        y = Y.ravel()
        m = int(np.min(X)) - 1
        M = int(np.max(X)) + 1
        if self._agraph is not None:
            self.clear_all()
            self.remove_widget(self._agraph)
        self._agraph = Graph(xlabel=xlabel,
                             ylabel=ylabel,
                             x_ticks_minor=0,
                             x_ticks_major=1,
                             y_ticks_major=0.1,
                             y_grid_label=True,
                             x_grid_label=True,
                             padding=5,
                             xlog=False,
                             ylog=False,
                             x_grid=True,
                             y_grid=True,
                             xmin=x_min,
                             xmax=x_max,
                             ymin=y_min,
                             ymax=y_max,
                             **self._graph_theme)

        u = np.unique(y)
        for x,i in enumerate(u):
            # self._agraph._clear_buffer()
            Xi = X[y == i, :]
            Xi = Xi[Xi[:, 0].argsort()]
            Xi = Xi[:, :2]
            Xi = list(map(tuple, Xi))
            plot = ScatterPlot(color=collors[x], _radius=10, points=Xi)
            self._agraph.add_plot(plot)
        self.add_widget(self._agraph)