#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Machine Learning Controlers - Classical Gesture Controler
# Made for Phyton 3.8 - Kivy 2.0
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

import numpy as np
import pandas as pd
from collections import deque
from sklearn.preprocessing import StandardScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.svm import SVC


class DataHandler():
    def __init__(self,**kwargs):
        self.features_all = ["Root_mean_square","Mean_Absolute_Value","Slope_Sign_Change","Waveform_length","Zero_Crossings"]
        self.prepross_all = ["Absolute Value","Envelop Data"]
        self.features_activated = list()
        self.prepross_activated = list()
        self.dict_funcs = {"Root_mean_square": self.Root_mean_square,     
                                "Mean_Absolute_Value": self.Mean_Absolute_Value,
                                "Slope_Sign_Change": self.Slope_Sign_Change,
                                "Waveform_length": self.Waveform_length,
                                "Zero_Crossings": self.Zero_Crossings,
                                "Absolute Value": self.Absolute_Value,
                               "Envelop Data": self.Envelop_Data}
        self.clfs_avaliable = ["Linear Discriminant Analisys","Suport Vector Machine"]                                      
        self.Dataset_Loaded = False
        self.percentage = [0,1]
    
    def Load_dataset(self,path):
        try:
            self.Raw_Data = pd.read_csv(path,index_col=0)
            if  "Class" in self.Raw_Data.columns:
                self.Raw_Data["Class"] -= 1 # arruma para começar em 0
                self.Raw_Data = self.Raw_Data.iloc[0:10000] #For Test Purposes-------------
                self.Dataset_Loaded = True
                return not self.Dataset_Loaded
            else:
                return not self.Dataset_Loaded
        except:
            return not self.Dataset_Loaded

    def Epoch_Data(self):
        self.Epoched_data,self.Epoched_Classes = list(), list()
        count = 0
        for x in range(self.Raw_Data.shape[0]):
            if self.Raw_Data["Class"][count] != self.Raw_Data["Class"][x]:
                self.Epoched_data.append(self.Raw_Data.loc[count:x-1].copy())
                self.Epoched_Classes.append(self.Raw_Data["Class"][count])
                count = x

    def Select_Classes(self,classes_selected):
        temp = list()
        for x,data in enumerate(self.Epoched_data):
            if classes_selected[int(data["Class"].iloc[0])]:
                temp.append(self.Epoched_data[x])
        self.Epoched_data = temp
 
    def Preprosses_Data(self):
        for x,data in enumerate(self.Epoched_data):
            data = np.array(data.drop(columns=["Subject","Class"]))
            for ch in range(data.shape[1]):
                for elem in self.prepross_activated:
                    data[:,ch] = self.Call_funcs(elem,data[:,ch])
            (self.Epoched_data[x]).loc[:,'emg0':'emg7'] = data

    def Feature_Data(self):
        self.Featured_Data = np.empty(((len(self.features_activated)*8)+2,0))
        self.percentage[0] = len(self.Epoched_data)
        count = 0
        for x,data in enumerate(self.Epoched_data):
            temp = np.array([data.iloc[0]["Class"],data.iloc[0]["Subject"]])
            data = data.drop(columns=["Subject","Class"])
            for elem in self.features_activated:
                t = self.Call_funcs(elem,data)
                temp = np.append(temp,t)     
            self.Featured_Data = np.append(self.Featured_Data,np.expand_dims(temp,axis=1),axis=1)
            count = x
            self.percentage[1] = count
        self.Standartize_Data()
    
    def Standartize_Data(self):
        self.scaler = StandardScaler()
        self.Featured_Data[2:,:] = self.scaler.fit_transform(self.Featured_Data[2:,:].T).T
    
    def Train_Classifier(self,classifier):
        X = self.Featured_Data[2:,:].T
        Y = self.Featured_Data[0,:]
        if classifier == self.clfs_avaliable[0]:
            clf = LDA()
        else:
            clf = SVC(kernel='rbf',gamma='scale')
        self.clf = clf.fit(X,Y)

    #======== Test Functions ============

    def Preprosses_Test(self,data):
        for ch in range(data.shape[1]):
                for elem in self.prepross_activated:
                    data[:,ch] = self.Call_funcs(elem,data[:,ch])
        return data            

    def Calculate_Features_test(self,data):
        test = pd.DataFrame(data=data)
        temp = np.empty(0)
        for elem in self.features_activated:
            t = self.Call_funcs(elem,test)
            temp = np.append(temp,t)
        return temp

    def Standartize_Test(self,data):
        self.data = self.scaler.transform(data.reshape(1,-1))
        return data

    def Test_Classifier(self,features):
        return self.clf.predict(features.reshape(1,-1))
        

    #======== Suport Functions ============

    def Call_funcs(self,elem,data):
        """Calls Feature functions based on dictionary reading""" 
        func_to_call = self.dict_funcs[elem]
        result = func_to_call(data)
        return result

        
    def Update_All_Selections(self,Preact,Featact):
        """Update selected Preprocessing steps and Features selected"""
        self.prepross_activated.clear()
        for x,y in enumerate(self.prepross_all):
            if Preact[x]:
                self.prepross_activated.append(y)        
        self.features_activated.clear()
        for x,y in enumerate(self.features_all):
            if Featact[x]:
                self.features_activated.append(y)

    def Absolute_Value(self,data):
        """
        Calculate the absolute value of a array

        :data: array of values -- np.array 1D
        :return: array of values -- np.array 1D
        """
        np.absolute(data)
        return data

    def Envelop_Data(self,series):
        """
        Envelops a time series via convolution, maintaining length

        :series: Time series -- np.array 1D
        :return: Enveloped Time series -- np.array 1D, same length as series
        """
        W = 100
        box = np.ones((W,))/W
        conv =  np.convolve(series, box, mode='same')
        return conv.T        

    def Root_mean_square(self,data_EMG): # Hassan, H. F., Abou-Loukh, S. J., & Ibraheem, I. K. (2019)
        rms = []
        for x in range(8):
            data = data_EMG.iloc[:,x]
            data = data ** 2
            su = data.sum()
            su = su/len(data)
            su = np.sqrt(su)
            rms.append(su)
        return rms
    
    # def Root_mean_square(self,EMG):
    #     """
    #     Calculates de Root Mean Square Feature as defined in  Hassan, H. F., Abou-Loukh, S. J., & Ibraheem, I. K. (2019)

    #     :EMG: EMG time series -- np.array 2D (n_samples,n_channels)
    #     :return: Featutre vector -- np.array 1D (n_channels)
    #     """
    #     EMG**2
    
    def Mean_Absolute_Value(self,data_EMG): # Hassan, H. F., Abou-Loukh, S. J., & Ibraheem, I. K. (2019)
        mav = []
        for x in range(8):
            data = np.abs(data_EMG.iloc[:,x])
            su = data.sum()
            su = su/len(data)
            mav.append(su)
        return mav
        
    def Slope_Sign_Change(self,data_EMG): # Hassan, H. F., Abou-Loukh, S. J., & Ibraheem, I. K. (2019)
        ssc = []
        for x in range(8):
            data = data_EMG.iloc[:,x]
            to = 0
            for i in range(1,len(data)-1):
                su = (data.iloc[i] - data.iloc[i-1])*(data.iloc[i] - data.iloc[1+1])
                to = to + np.abs(su)
            ssc.append(to)
        return ssc

    def Waveform_length(self,data_EMG): # Hassan, H. F., Abou-Loukh, S. J., & Ibraheem, I. K. (2019)
        wl = []
        for x in range(8):
            data = data_EMG.iloc[:,x]
            to = 0
            for i in range(len(data)-1):
                su = data.iloc[i+1] - data.iloc[i]
                to = to + np.abs(su)
            wl.append(to)
        return wl

    def Zero_Crossings(self,data_EMG):
        zc = []
        for x in range(8):
            data = data_EMG.iloc[:,x]
            to = 0
            for i in range(len(data)-1):
                if((data.iloc[i]*data.iloc[i+1]) < 0 ):
                    sng = 1
                else:
                    sng = 0
                to = to + sng
            zc.append(to)
        return zc