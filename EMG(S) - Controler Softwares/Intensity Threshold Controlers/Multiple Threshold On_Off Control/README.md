# Multiple On Off Controler via Thresholds

## Description

In this program we explore how to augment the fredom of controll not by improving the controler logic but by using more tham one sensor. While tecnicaly speaking the On Off Threshold Controler and Proportional Controler use all of the 8 avaliable detectors the way they are implemented (using a average of the detectors) works like a single detector. This example explores the use of diferent detectors independently taking advantage of their positioning to generate a more complex control signal. The idea is that 3 diferent sensors are chosed based on the previous knowlege that diferent hand movements and gestures contract diferent muscles in the arm and in turn create more signal on some detectors than others. For example with your arm resting on the lab and the hand facing up. If you perform a flexion of the hand towards the arm the muscle called flexor carpi radialis, on the anterior portion of the arm, becomes very visible. Making the oposity movement (extention) has a diferent effect making the muscle called extensor digitorum, on the posterior(or backside), contract. As the localization of these muscles are diferent only some of the Myo sensors will properly sense theses contractions. As such a system with multiple thresholds, for diferent sensors, can be constructed so each will be activated only during certain movements. This concep is one of the simplest possible ways to perform a "classification" of gestures with minimal processing.

This program allow the user to sellect the 3 sensors used. The sugestion is to use the the flexion and extention of the palm and the opening of hand as the three movements as they use diferent muscles arroung the circunference of the arm. De exact number of the sensors depends on the positioning of the armband on the arm. Our sugestion is to position the Armand so the micro Usb charging port is aligned with the middle inder side of the elbow and in this position the sensors x,x and x tend so represent the flexion of the palm, extantion of the palm and the opening of the hand, respectively.

## Dependencies

* kivy
* kivy_garden
* pandas
* numpy
* pyserial
* myo-python
