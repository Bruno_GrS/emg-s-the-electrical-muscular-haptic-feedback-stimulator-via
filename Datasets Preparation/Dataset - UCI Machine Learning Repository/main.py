#
# EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
# Dataset Preparation Script - UCI Machine Learning Repository
# Made for Phyton 3.8
# Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
# <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
# Licensed under the MIT License. See License.txt in root.
# 

### Tranforms the UCI Machine Learning Repository in the right format .csv file used by the other programs on this repository.
# To use this code follow the steps bellow:
# 1) Donwload the dataset at: https://archive.ics.uci.edu/ml/datasets/EMG+data+for+gestures# as a .zip file
# 2) Unpack the EMG_data_for_gestures-master folder on a temporary location 
# 3) Chose any number of subjects folders and copy them to the same folder as this script (main.py).
# Each folder Contains 2 .txt files, one for each session, you may include only one or both.
# 4) Run this script
# 5) A file called EMG_Test_Data.csv should appear on the same folder. This is the file to be used on the other programs of this repository.

import numpy as np
import os
import pandas as pd

if __name__ == '__main__':
    path = '.'
    directory_contents = os.listdir(path) #Lists all content of the directory
    i = 0
    subjects = list()

    # Finds all subjects and sessions in the folder
    for x in directory_contents:
        if x.isnumeric():
            subjects.append(x)

    # Gets all data for the present subjects in each folder
    for x in subjects:
        dire = x
        files = os.listdir(dire)
        for y in files:
            path = dire + "\\"+ y
            t = pd.read_csv(path, sep="\t").astype(float)
            t["Subject"] = np.full(t.shape[0],int(dire[-1]),dtype=int)
            if "data" in locals():
                data = data.append(t, ignore_index = True)
            else:
                data = t

    # Excludes Classes 0 (undefined) and 7 (not present in all Subjects)
    data = data.astype({'class': 'int'})
    data = data[data["class"] != 0]
    data = data[data["class"] != 7] # IF YOU WANT TO INCLUDE CLASS 7 COMENT THIS LINE!!!!!

    # Create the necessary dataframe
    col = ["emg0","emg1","emg2","emg3","emg4","emg5","emg6","emg7","Class","Subject"]
    df = pd.DataFrame()

    # Arranges the data in correct format
    for y,x in enumerate(col):
        df[x] = data.iloc[1:-1,y+1].astype(float)
    df = df.astype({'Class': 'int','Subject': 'int'})

    # Saves final dataframe
    df.to_csv("EMG_Test_Data") 
