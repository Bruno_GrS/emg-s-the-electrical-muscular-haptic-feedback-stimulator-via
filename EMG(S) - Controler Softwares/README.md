# Control Methods via sEMG signals for Human - Machine Interfaces

## Control Methods? Human - Machine Interfaces?

Nowadays interacting with some sort of computer systems is a ubctuous dayly life fenomena. Most people do it and incontable ammount of times every day for a variety of reasons in a variety of diferent csenarious. The study of such interactions is sometimes called Human-Machine interfaces, specialy when talking about more "unusual" methods of interaction such as interacting via EMG signal or maybe EEG(Brain signals - normaly called Brain Computer Interface). Other way of understanding this is thinking that the signal in question (in our case the EMG) is being used as a controler, a mouse for example, and what we are doing is analasing the signal and generating diferent signals for diferent "types" of EMG signal.

## Methods of Control

This repository is a collection of small programs that were made to Ilustrate some examples of control strategies. More presisely this repository has 6 example programs that employ diferent strategies to recive, in real time, the EMG readings from a Myo Armband than transform this signals in an output/Control signal that is sent to a EMS system generating a "feedback" to the user. Bellow the logic of this comunication is draw.

![Alt text](https://i.imgur.com/cVblhfD.png)

## The Programs avaliable

As said this repository contains 6 examples of diferent strategies. The 6 examples can be separeted in two distinc groups we will call the Intensity Threshold aproaches and the Machine Learning aproaches each with 3 of the 6 diferent examples.

### Intensity Threshold Aproaches

We called these aproaches Intensity Treshold as they are all based on some sort of Threshold that is put on the Intesity reading of the EMG signal. This type of aproach to control has its positives and negatives. On the positive side they are easy to implement and easy for the users to understand an use, while also requiring very little processing of the data making it ideal for low power IoT devices. However its beggest advantage is that they, arguabily, require no prior training (aside from adjusting the Treshold value). On the other side this aproach allows for few possible outputs resulting in a small degree of freedom, making complex control timeconsuming and unintuitive.

* [On-Off Threshold Controler](https://bitbucket.org/Bruno_GrS/myo_estim/src/master/Kivy-Based-Programs/On_Off%20Threshold%20Control/)

    * Simple Controler that uses a Threshold on the signal SUM of all 8 sensors to define and On and a Off state.

* [Proportional Controler](https://bitbucket.org/Bruno_GrS/myo_estim/src/master/Kivy-Based-Programs/Proportional%20Control/)

    * Simple Controler that defines a Minimum a a Maximum value for the signal intensity an outputs a signal proportional to the intensity of the EMG SUM signal.

* [Multiple On-Off Threshold Controlers](https://bitbucket.org/Bruno_GrS/myo_estim/src/master/Kivy-Based-Programs/Multiple%20Threshold%20On_Off%20Control/)

    * Similarly to the On-Off Threshold Controler this program defines a Treshold that defines two possible states (On and Off), but this time it is done on diferent sensors allowing for more degress of control.

### Machine Learning Aproaches

The Machine Learning Aproaches are called this because they all rely in some Machine powered algoritm that recives the input EMG signal, process it and decides it bellongs to a certain classification via some sort of comparison. In a sense this aproaches have directely oposite positives and negatives, being harder to implement and to explaing to users, while also requiring significantily more processing power making some harder do deploy in real world. And specialy they require, somethimes, large ammounts of training data that need to be similar and meningully represent similar information as it will recive during real time operation. On the other hand, all this incressed complexity allows for a creat increasse in the possible degrees of freadom of the system. Techinicaly speaking any of these approaches can be implemented for a incontable large number of possible classes allowing for infinitely complex output signals and freadom. On real world examples this is clearly not true and many problems arise with larger number of classes, hoever they still allows for considerable more degrees of freadom.

* [Classical Gesture Controler](https://bitbucket.org/Bruno_GrS/myo_estim/src/master/Kivy-Based-Programs/)

    * Simple 

* [Convolution Neural Network Controler](https://bitbucket.org/Bruno_GrS/myo_estim/src/master/Kivy-Based-Programs/)

    * Simple

* [Hyperdimensional Computing Controlers](https://bitbucket.org/Bruno_GrS/myo_estim/src/master/Kivy-Based-Programs/)

    * Similarly

## Thechinical Considerations

All programs contanied here are made in python mainly with the Support of Kivy as its GUI interface.
They where made and tested primarly for the Windows plataform on the Python 3.8 release.
All support libraries can be obtained via pip and are described on each individual README file.

### Licence
MIT License

Copyright (c) 2020 Bruno Grandi Sgambato

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Some Parts of this Work modifications on a work Licensed (MIT license) and Copyrighted by Max Pfeiffer and Tim Dünte and marked accordingly.

### Disclaimer
THIS REPOSITORY IS FOR INFORMATIONAL PURPOSES ONLY. ALL THE INFORMATION AND CODE PROVIDED IS PROVIDED "AS-IS" AND WITH NO WARRANTIES. NO EXPRESS OR IMPLIED WARRANTIES OF ANY TYPE, INCLUDING FOR EXAMPLE IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, ARE MADE. EMG AND EMS CARRY RISK KNOW AND UNKNOW TO THE HUMAN BODY. THIS PROJECT AND ITS PARTS ARE NOT PROOFED FOR MEDICAL USE. IF YOU NOT KNOW WHAT YOU ARE DOING, DO NOT USE THIS PROJECT.
