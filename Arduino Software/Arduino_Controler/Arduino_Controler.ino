/**
* EMG(S) - The Electrical Muscular Haptic Feedback Stimulator via Electromyography
* Arduino Controler
* Made for Ardunio BLE 33
* Copyright 2020 by Bruno Grandi Sgambato <brunog.sgambato@gmail.com>
* <https://bitbucket.org/Bruno_GrS/emg-s-the-electrical-muscular-haptic-feedback-stimulator-via/src/main/>
* Licensed under the MIT License. See License.txt in root.
* 
* Based on ArduinoSoftware_Arduino_IDE
*  Copyright 2016 by Tim Duente <tim.duente@hci.uni-hannover.de>
*  Copyright 2016 by Max Pfeiffer <max.pfeiffer@hci.uni-hannover.de>
*  <https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home>
*  Licensed under "The MIT License (MIT) 'military use of this product is forbidden' V 0.2"
*/

/**
 * Minor Revisions to this file by Pedro Lopes <plopesresearch@gmail.com>, all credit remains with the original authors (see above).
 */

// Necessary files (AltSoftSerial.h, AD5252.h, Rn4020BTLe.h, EMSSystem.h, EMSChannel.h) and dependencies (Wire.h, Arduino.h)
#include "ArduinoSoftware.h"
#include "Wire.h"
#include "AD5252.h"
#include "EMSSystem.h"
#include "EMSChannel.h"
#include "Debug.h"

//setup for accepting commands also via USB (accepts USB commands if ACCEPT_USB_COMMANDS is 1)
#define ACCEPT_USB_COMMANDS 1

//Initialization of control objects
AD5252 digitalPot(0);
EMSChannel emsChannel1(5, 4, A2, &digitalPot, 1);
EMSChannel emsChannel2(6, 7, A3, &digitalPot, 3);
EMSSystem emsSystem(2);

String command = "";

void setup() {
	Serial.begin(115200);

	//debug_println(F("\nSETUP:"));
	//debug_println(F("\tBT: INITIALIZED"));

	//Add the EMS channels and start the control
	//debug_println(F("\tEMS: INITIALIZING CHANNELS"));
	emsSystem.addChannelToSystem(&emsChannel1);
	emsSystem.addChannelToSystem(&emsChannel2);
	EMSSystem::start();
	//debug_println(F("\tEMS: INITIALIZED"));
	//debug_println(F("\tEMS: STARTED"));
	//debug_println(F("SETUP DONE"));
  debug_println(F("E"));
	emsSystem.shutDown();

	command.reserve(21);
}

String hexCommandString;
const String BTLE_DISCONNECT = "Connection End";

void loop() {
	//Checks whether a signal has to be stoped
	if (emsSystem.check() > 0) {

	}

	//Communicate to the EMS-module over USB
	if (ACCEPT_USB_COMMANDS) {
		if (Serial.available() > 0) {
    
			 String command = Serial.readStringUntil('\n');
       debug_println(command);
			 emsSystem.doCommand(&command);


//			char c = Serial.read();
//			debug_Toolkit(c);

		}
	}

}

//Convert-functions for HEX-Strings "4D"->"M"
int convertTwoHexCharsToOneByte(String &s, uint8_t index) {
	int byteOne = convertHexCharToByte(s.charAt(index));
	int byteTwo = convertHexCharToByte(s.charAt(index + 1));
	if (byteOne != -1 && byteTwo != -1)
		return (byteOne << 4) + byteTwo;
	else {
		return -1;
	}
}

int convertHexCharToByte(char hexChar) {
	if (hexChar >= 'A' && hexChar <= 'F') {
		return hexChar - 'A' + 10;
	} else if (hexChar >= '0' && hexChar <= '9') {
		return hexChar - '0';
	} else {
		return -1;
	}
}

//For testing
void debug_Toolkit(char c) {
	if (c == '1') {
		if (emsChannel1.isActivated()) {
			emsChannel1.deactivate();
			debug_println(F("\tEMS: Channel 1 inactive"));
		} else {
			emsChannel1.activate();
			debug_println(F("\tEMS: Channel 1 active"));
		}
	} else if (c == '2') {
		if (emsChannel2.isActivated()) {
			emsChannel2.deactivate();
			debug_println(F("\tEMS: Channel 2 inactive"));
		} else {
			emsChannel2.activate();
			debug_println(F("\tEMS: Channel 2 active"));
		}
	} else if (c == 'q') {
		digitalPot.setPosition(1, digitalPot.getPosition(1) + 1);
		debug_println(
				String(F("\tEMS: Intensity Channel 1: "))
						+ String(digitalPot.getPosition(1)));
	} else if (c == 'w') {
		digitalPot.setPosition(1, digitalPot.getPosition(1) - 1);
		debug_println(
				String(F("\tEMS: Intensity Channel 1: "))
						+ String(digitalPot.getPosition(1)));
	} else if (c == 'e') {
		//Note that this is channel 3 on Digipot but EMS channel 2
		digitalPot.setPosition(3, digitalPot.getPosition(3) + 1);
		debug_println(
				String(F("\tEMS: Intensity Channel 2: "))
						+ String(digitalPot.getPosition(3)));
	} else if (c == 'r') {
		//Note that this is channel 3 on Digipot but EMS channel 2
		digitalPot.setPosition(3, digitalPot.getPosition(3) - 1);
		debug_println(
				String(F("\tEMS: Intensity Channel 2: "))
						+ String(digitalPot.getPosition(3)));
	}
}
